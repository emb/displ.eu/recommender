import logging
import tiktoken
import json
from openai import OpenAI
from dotenv import load_dotenv  
import os

load_dotenv()

API_KEY = os.getenv('OPENAI_API_KEY')

MODEL = "gpt-4o-mini"
MAX_TOKEN = 16000
MAX_ITEMS_PER_FILE = 25000

LANGUAGE_CODES = {
    "English": "en",
    "German": "de",
    "Croatian": "hr",
    "Serbian": "sr",
    # "Bosnian": "bs", # Croatian and Bosnian are very similar, so only run Croatian
    "Czech": "cs",
    "Slovakian": "sk",
    "French": "fr",
    "Hungarian": "hu",
    "Italian": "it",
    "Polish": "pl",
    "Spanish": "es",
    "Dutch": "nl",
    "Portuguese": "pt",
    "Ukrainian": "uk",
    "Russian": "ru",
    "Turkish": "tr",
    "Romanian": "ro",
    "Bulgarian": "bg",
    "Finnish": "fi",
    "Greek": "el",
    "Swedish": "sv"
    }

PROMPT_KEEP_HTML_TRANSLATE = """
Your task is to translate the provided text into **{TARGET_LANGUAGE}**, 
while preserving the HTML formatting, CSS styles, and any JavaScript code embedded within the text. 
Additionally, maintain all whitespace characters such as line breaks (`\\n`). 
If the input text is already in **{TARGET_LANGUAGE}**, repeat the text exactly as it is.

**Instructions:**

- **Translate the text into {TARGET_LANGUAGE}.** If the text is already in {TARGET_LANGUAGE}, repeat it word for word.
- **Preserve all HTML tags and formatting** in their original positions within the text.
- **Preserve all CSS styles** within `<style>` tags without any modifications.
- **Preserve all JavaScript code** within `<script>` tags without any modifications.
- **Preserve all whitespace characters**, including line breaks (`\\n`), exactly as they appear in the input.

**Constraints:**

- Do **not** add any text.
- Do **not** change any content or alter the meaning.
- Do **not** provide any explanations, comments, or additional information.
- Do **not** shorten or summarize the text.

**Output:**

- Only output the translated text, preserving the HTML formatting, CSS styles, JavaScript code, and whitespace as specified.
"""

def num_tokens_from_string(string: str, encoding_name: str) -> int:
    try:
        encoding = tiktoken.encoding_for_model(encoding_name)
        num_tokens = len(encoding.encode(string))
        return num_tokens
    except Exception as e:
        logging.error(f"Error in num_tokens_from_string: {e}")
        return 0
    
    
def create_batch_item(custom_id, content, target_language_name, target_language_code, model, max_tokens, template):
    try:
        return {
            "custom_id": f"{custom_id}_{target_language_code}",
            "method": "POST",
            "url": "/v1/chat/completions",
            "body": {
                "model": model,
                "messages": [
                    {"role": "system", "content": template.format(TARGET_LANGUAGE=target_language_name)},
                    {"role": "user", "content": content}
                ],
                "max_tokens": max_tokens,
                "temperature": 0,
                "top_p": 1,
                "frequency_penalty": 0,
                "presence_penalty": 0,
                "seed": 42
            }
        }
    except Exception as e:
        logging.error(f"Error in create_batch_item: {e}")
        return {}   
    

def create_teaser_translation_data(jsonl_input_file_path: str, jsonl_output_file_path: str):
    total_tokens = 0
    custom_data = []
    teasers_data = []
    # with open(jsonl_input_file_path, 'r', encoding='utf-8') as file:
    #     for line in file:
    #         data = json.loads(line)
    #         custom_id = data.get('custom_id')
    #         content = data.get('response', {}).get('body', {}).get('choices', [{}])[0].get('message', {}).get('content')
    #         if custom_id and content:
    #             teasers_data.append({
    #                 'custom_id': custom_id,
    #                 'content': content
    #             })

    with open(jsonl_input_file_path, 'r', encoding='utf-8') as file:
        teasers_data = json.load(file)
                
    for teaser in teasers_data:
        for key, value in LANGUAGE_CODES.items():
            target_language_name = key
            target_language_code = value
            total_tokens += num_tokens_from_string(teaser['content'], MODEL)
            batch_item = create_batch_item(teaser['custom_id'], teaser['content'], target_language_name, target_language_code, MODEL, MAX_TOKEN, PROMPT_KEEP_HTML_TRANSLATE)
            custom_data.append(batch_item)

    print("TOTAL TOKEN(INPUT):", total_tokens)
    costs_per_token_in = 0.150   # USD per million tokens
    costs_per_token_out = 0.600  # USD per million
    costs = total_tokens * (costs_per_token_in + costs_per_token_out) / (10**6)
    print("COST ESTIMATE with 50% Batch discount: ", costs / 2.0, "$")
            
    with open(jsonl_output_file_path, 'w', encoding='utf-8') as file:
        for item in custom_data:
            file.write(json.dumps(item) + '\n')
    logging.info("Created batch request file:", jsonl_output_file_path)
    
    return custom_data

# Function to split a JSONL file into smaller parts with a maximum of 25000 items each
def split_jsonl_file(data, output_prefix, max_items=25000):
    split_files = []
    current_split = 1
    current_count = 0
    split_filename = f"{output_prefix}_part{current_split}.jsonl"
    split_f = open(split_filename, 'w', encoding='utf-8')
    split_files.append(split_filename)

    for item in data:
        json.dump(item, split_f, ensure_ascii=False)
        split_f.write('\n')
        current_count += 1
        if current_count >= max_items:
            split_f.close()
            current_split += 1
            split_filename = f"{output_prefix}_part{current_split}.jsonl"
            split_f = open(split_filename, 'w', encoding='utf-8')
            split_files.append(split_filename)
            current_count = 0
    split_f.close()
    return split_files
            

def run_teaser_translation_batch(jsonl_input_file_path: str, jsonl_output_file_path: str, output_jsonl_split_prefix: str):
    custom_data = create_teaser_translation_data(jsonl_input_file_path, jsonl_output_file_path)
    
        # Initialize OpenAI client
    client = OpenAI(api_key=API_KEY)
    
    if len(custom_data) <= MAX_ITEMS_PER_FILE:
        batch_input_file = client.files.create(
            file=open(jsonl_output_file_path, "rb"),
            purpose="batch"
        )
        batch_input_file_id = batch_input_file.id
        batch_job = client.batches.create(
                input_file_id=batch_input_file_id,
                endpoint="/v1/chat/completions",
                completion_window="24h",
                metadata={
                    "description": "nightly teaser translation job"
                }
        )
        print("Created batch job:", batch_job)
        
    else:

        split_files = split_jsonl_file(custom_data, output_jsonl_split_prefix, max_items=MAX_ITEMS_PER_FILE)
        # Create batch jobs for each split file
        
        for split_file in split_files:
            try:
                batch_input_file = client.files.create(
                    file=open(split_file, "rb"),
                    purpose="batch"
                )
                batch_input_file_id = batch_input_file.id
                batch_job = client.batches.create(
                    input_file_id=batch_input_file_id,
                    endpoint="/v1/chat/completions",
                    completion_window="24h",
                    metadata={
                        "description": f"Batch job for {split_file}"
                    }
                )
                print(f"Created batch job for {split_file}: {batch_job}")

            except Exception as e:
                print(f"Failed to create batch job for {split_file}: {e}")
    
    
if __name__ == "__main__":
    # short_jsonl_input_file_path = 'data/teaser/50_100_version/batch_67b8393a83b8819091cc6b471b44ef35_output.jsonl'
    # short_jsonl_output_file_path = 'data/teaser/50_100_version/SHORT_teasers_translated_batch.jsonl'
    # short_jsonl_output_split_prefix = 'data/teaser/50_100_version/SHORT_teasers_translated_batch_split'
    
    # long_jsonl_input_file_path = 'data/teaser/50_100_version/batch_67b8393c07ec81909e97a4705d136532_output.jsonl'
    # long_jsonl_output_file_path = 'data/teaser/50_100_version/LONG_teasers_translated_batch.jsonl'
    # long_jsonl_output_split_prefix = 'data/teaser/50_100_version/LONG_teasers_translated_batch_split'

    short_jsonl_input_file_path = 'data/teaser/output/keep_same/no_summarization_SHORT_INTRO.json'
    short_jsonl_output_file_path = 'data/teaser/SHORT_unchanged_translated_batch.jsonl'
    short_jsonl_output_split_prefix = 'data/teaser/SHORT_unchanged_translated_batch_split'

    long_jsonl_input_file_path = 'data/teaser/output/keep_same/no_summarization_LONG_INTRO.json'
    long_jsonl_output_file_path = 'data/teaser/LONG_unchanged_translated_batch.jsonl'
    long_jsonl_output_split_prefix = 'data/teaser/LONG_unchanged_translated_batch_split'

    run_teaser_translation_batch(short_jsonl_input_file_path, short_jsonl_output_file_path, short_jsonl_output_split_prefix)
    run_teaser_translation_batch(long_jsonl_input_file_path, long_jsonl_output_file_path, long_jsonl_output_split_prefix)

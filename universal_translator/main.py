import argparse
import logging
import os
import sys
from openai import OpenAI
from app import (
    fetch_from_database,
    preprocess_batch,
    send_batch,
    scrape_result,
    import_to_database,
)
from app.constant import API_KEY, MODEL, MAX_TOKENS, PROMPT_KEEP_HTML_TRANSLATE, INPUT_JSONL_FILE_PATH

# Configure logging with timestamp
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


def parse_args():
    """
    Parse command-line arguments and subcommands.
    """
    parser = argparse.ArgumentParser(
        description="Step-based processing for REPCO content items"
    )
    # Global parameter shared by some steps (e.g., language can be used as both target language and as a temporary file name)
    parser.add_argument('--language', type=str, default="all", help="Language to translate to (and used as temporary file name)")

    subparsers = parser.add_subparsers(dest="command", help="Sub-command to execute")

    # Subcommands for each step
    subparsers.add_parser("full", help="Run the full process workflow")
    subparsers.add_parser("fetch", help="Only fetch content items from the database")
    subparsers.add_parser("preprocess", help="Only preprocess batch data and create a JSONL file")
    subparsers.add_parser("send", help="Only send the batch request to OpenAI API")
    subparsers.add_parser("scrape", help="Only scrape and process the JSONL result file")
    subparsers.add_parser("import", help="Only import the processed results into the database")

    args = parser.parse_args()
    if args.command is None:
        args.command = "full"
    return args


def step_fetch():
    """
    Step 1: Fetch content items from the REPCO database.
    """
    logging.info("Starting fetch step")
    content_items = fetch_from_database.export_latest_content_items()
    if not content_items:
        logging.info("No content items to process in the fetch step.")
    else:
        logging.info("Fetched %d content items from the database.", len(content_items))


def step_preprocess(language):
    """
    Step 2: Preprocess batch data (write items to a JSONL batch file).
    The language parameter is used for configuration (and here, assumed to be the name of the temporary file).
    """
    logging.info("Starting preprocess step")
    prep_content_items = preprocess_batch.preprocess_batch_data(language, MODEL, MAX_TOKENS, PROMPT_KEEP_HTML_TRANSLATE)
    if not prep_content_items:
        logging.info("No content items to process in the preprocess step.")
    else:
        logging.info("Preprocessed %d content items. Exported to %s", len(prep_content_items), INPUT_JSONL_FILE_PATH)


def step_send(client):
    """
    Step 3: Send the JSONL batch request to the OpenAI API.
    """
    logging.info("Starting send batch step")
    send_batch.send_batch_request(client)
    logging.info("Send batch step complete.")


def step_scrape(client):
    """
    Step 4: Scrape/process the JSONL result file.
    """
    logging.info("Starting scrape step")
    scrape_result.process_batch_jobs(client)
    scrape_result.retry_pending_batch_jobs(client)
    logging.info("Scrape step complete.")


def step_import():
    """
    Step 5: Import processed results into the database.
    """
    logging.info("Starting import step")
    import_to_database.run_import_to_database()
    logging.info("Import step complete.")


def run_full_workflow(args, client):
    """
    Run the entire workflow in sequence.
    """
    logging.info("Starting full workflow")

    # Step 1: Fetch items (and log the count)
    content_items = fetch_from_database.export_latest_content_items()
    if not content_items:
        logging.info("No content items to process. Exiting full workflow.")
        return
    logging.info("Fetched %d content items.", len(content_items))

    # Step 2: Preprocess the batch data (create a JSONL file)
    preprocess_batch.preprocess_batch_data(args.language, MODEL, MAX_TOKENS, PROMPT_KEEP_HTML_TRANSLATE)

    # Step 3: Send the batch request to OpenAI API
    send_batch.send_batch_request(client)

    # Step 4: Scrape/process the JSONL result file
    scrape_result.process_batch_jobs(client)
    scrape_result.retry_pending_batch_jobs(client)

    # Step 5: Update/Import processed results back into the REPCO database
    import_to_database.run_import_to_database()

    # Clean up temporary file (assuming the preprocessed file uses args.language as its filename)
    if os.path.exists(args.language):
        os.remove(args.language)
        logging.info("Temporary file '%s' removed.", args.language)

    logging.info("Full workflow finished successfully.")


def main():
    args = parse_args()
    client = OpenAI(api_key=API_KEY)

    if args.command == "fetch":
        step_fetch()
    elif args.command == "preprocess":
        step_preprocess(args.language)
    elif args.command == "send":
        step_send(client)
    elif args.command == "scrape":
        step_scrape(client)
    elif args.command == "import":
        step_import()
    elif args.command == "full":
        run_full_workflow(args, client)
    else:
        logging.error("Unknown command: %s", args.command)
        sys.exit(-1)


if __name__ == "__main__":
    main()

import re
import html
from bs4 import BeautifulSoup
# from langchain_experimental.text_splitter import SemanticChunker
# from langchain_openai.embeddings import OpenAIEmbeddings
# from langchain_community.embeddings import HuggingFaceEmbeddings

class TextPreprocessor:
    """
    A class to preprocess text by removing HTML tags, unescaping HTML entities,
    removing emojis, and normalizing whitespace.
    """
    def __init__(self):
        # Define emoji pattern
        self.emoji_pattern = re.compile(
            "[" 
            "\U0001F600-\U0001F64F"  # Emoticons
            "\U0001F300-\U0001F5FF"  # Symbols & Pictographs
            "\U0001F680-\U0001F6FF"  # Transport & Map Symbols
            "\U0001F1E0-\U0001F1FF"  # Flags
            "\U00002700-\U000027BF"  # Dingbats
            "\U0001F900-\U0001F9FF"  # Supplemental Symbols and Pictographs
            "]+", 
            flags=re.UNICODE
        )
        # self.hf_embeddings = OpenAIEmbeddings()
        # self.text_splitter = SemanticChunker(self.hf_embeddings)

    def preprocess(self, text: str) -> str:
        """
        Preprocess the input text by cleaning HTML, removing emojis, and normalizing whitespace.

        Args:
            text (str): The input text to preprocess.

        Returns:
            str: The preprocessed, clean text.
        """
        # Remove HTML tags using BeautifulSoup
        soup = BeautifulSoup(text, "html.parser")
        clean_text = soup.get_text(separator=' ')

        # Unescape HTML entities
        clean_text = html.unescape(clean_text)

        # Remove emojis
        clean_text = self.emoji_pattern.sub(r'', clean_text)
        
        # Remove special characters except basic punctuation
        clean_text = re.sub(r'[^\w\s.,!?-]', '', clean_text)

        # Normalize whitespace
        clean_text = re.sub(r'\s+', ' ', clean_text).strip()

        return clean_text
    
    # def semantic_chunk(self, text: str) -> str:
    #     """
    #     Semantic chunk the input text into smaller chunks.
    #     """
    #     docs = self.text_splitter.create_documents([text])
    #     return docs
        # print(len(docs))
    


# raw_text = """
# <p>ANCHORAGE\u00a0Krippenbauverein &#8222;Siegrieds Patschen&#8220;\u00a0&#x1f97f;\u00a0und das letzte Interview mit\u00a0In This Temple!\u00a0&#x1f399;&#xfe0f;&#x271d;&#xfe0f; &#x25b6;\u00a0Heute, Sonntag, 20:06 Uhr bei For Those About To Rock! &#x25b6;\u00a0Chris &amp; Kenny von\u00a0ANCHORAGE\u00a0und Jan von\u00a0In This Temple\u00a0im Interview mit\u00a0Aaron Olsacher &#x25b6; \u00a0PLAYLIST AUF SPOTIFY\u00a0&#x1f3a7; https://open.spotify.com/playlist/5V17W0fn2odv4fbnZRbk0R &#x25b6;\u00a0Mentioned and played: Peter Gordebeke Motley Crue KAISER FRANZ JOSEF The Gogets Stadtaffe Bar Heavy M\u00f6lltol Counterweight Official ((stereo)) Kill &#8230; <a title="Anchorage / In This Temple Interview | For Those About To Rock | Dec 15/2019" class="read-more" href="https://cba.media/436572">Weiterlesen <span class="screen-reader-text">Anchorage / In This Temple Interview | For Those About To Rock | Dec 15/2019</span></a></p>
# """

# preprocessor = TextPreprocessor()
# clean_text = preprocessor.preprocess(raw_text)
# print(clean_text)
# splitted_text = preprocessor.semantic_chunk(clean_text)
# print(splitted_text[0])
# print(splitted_text[1])

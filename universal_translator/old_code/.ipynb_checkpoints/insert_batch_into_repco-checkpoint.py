#!/usr/bin/env python3

import psycopg
from psycopg.rows import dict_row

import sys
import os

import pandas as pd
import json

from datetime import datetime

INSERT_SINGLE_FILE_ONLY=False


# Connect to PostgreSQL
conn = psycopg.connect(
    dbname='repco-live-10.2',
    user=os.getenv('DB_USER', 'repco'),
    password=os.getenv('DB_PASSWORD'),
    host=os.getenv('DB_HOST', 'localhost'),
    port=os.getenv('DB_PORT', '5432')
    )
print(conn)
cursor = psycopg.ClientCursor(conn, row_factory=dict_row)



def split_custom_id_column(df: pd.DataFrame, col_name: str = 'custom_id') -> pd.DataFrame:
    """
    Given a DataFrame containing a string column in the form 'uid_fieldname_language',
    split the column into three new columns: 'uid', 'fieldname', 'language'.

    :param df: The input pandas DataFrame.
    :param col_name: The name of the column to split. Defaults to 'custom_id'.
    :return: The modified DataFrame with three new columns.
    """
    # Split the string in the specified column by underscores into 3 parts
    # NEW:
    #   df[['uid', 'revisionId', 'fieldname', 'language']] = df[col_name].str.split('_', n=2, expand=True)
    df[['uid', 'revision', 'fieldname', 'src_lang', 'target_lang']] = df[col_name].str.split('_', expand=True)
    return df


def parse_jsonl_to_dataframe(file_path: str) -> pd.DataFrame:
    """Parse and convert to a pandas dataframe what we get from openAi's batch results.
    We get:

    ... example ...
    """
    data = []
    with open(file_path, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.strip()
            if not line:
                continue

            record = json.loads(line)
            custom_id = record.get("custom_id")

            choices = record.get("response", {}) \
                            .get("body", {}) \
                            .get("choices", [])

            for choice in choices:
                if choice.get("finish_reason") == "stop":
                    content = choice.get("message", {}).get("content")
                    data.append({
                        "custom_id": custom_id,
                        "content": content
                    })
                # elif: should we log somewhere that it did not work???? XXX

    # Build the dataframe
    df = pd.DataFrame(data, columns=["custom_id", "content"])
    df = split_custom_id_column(df)
    return df


def load_into_contentItemTranslation(df: pd.DataFrame):
    """INSERT or UPDATE (UPSERT) each translated row into the right row of 
    "ContentItemTranslation in repco-live-16.2"""

    SQL_TEMPLATE = """INSERT INTO "ContentItemTranslation" (
            "targetLanguage", 
            "{X}", 
            "contentItemUid", 
            "revisionId", 
            "createdAt", 
            "updatedAt", 
            engine, 
            metadata) 
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
        ON CONFLICT ("contentItemUid", "revisionId", "targetLanguage")
        DO UPDATE
            SET "targetLanguage" = %s,
            "{X}" = %s,
            "updatedAt" = %s,
            engine = %s,
            metadata = %s 
    """

    if 'fieldname' in df and df['fieldname'] == 'title':
        SQL = SQL_TEMPLATE.format(X='title')
        field = df['content']
    elif 'fieldname' in df and df['fieldname'] == 'summary':
        SQL = SQL_TEMPLATE.format(X='summary')
        field = df['content']
    elif 'fieldname' in df and df['fieldname'] == 'content':
        SQL = SQL_TEMPLATE.format(X='content')
        field = df['content']
    else: 
        print(f"Warning: did not find 'title', 'summary' nor 'content in the df. df = {df}")
        return

    missing_uids = set()
    now = datetime.today()
    engine = 'gpt-4o-mini-2024-07-18'
    metadata = None

    # Use an alias ('cnt') for the count so that we can access it by name
    check_existence_sql = """SELECT COUNT(*) AS cnt FROM "ContentItemTranslation" 
                             WHERE "contentItemUid" = %s AND "revisionId" = %s AND "targetLanguage" = %s"""
    
    cursor.execute(check_existence_sql, (df['uid'], df['revision'], df['target_lang']))
    result = cursor.fetchone()

    # Determine if an entry exists by checking the count
    if result is not None:
        try:
            # If the cursor returns a dict (e.g., RealDictCursor)
            count = result["cnt"]
        except (KeyError, TypeError):
            # Otherwise assume it's a tuple-like object
            count = result[0]
    else:
        count = 0

    exists = count > 0

    try:
        if not exists:
        #     # If it exists, perform an update
        #     cursor.execute(SQL, (df['target_lang'], df['content'], df['uid'], df['revision'], now, None, engine, metadata, df['target_lang'], field, now, engine, metadata))
        # else:
            # If it does not exist, perform an insert
            cursor.execute(SQL, (df['target_lang'], df['content'], df['uid'], df['revision'], now, None, engine, metadata, df['target_lang'], field, now, engine, metadata))
    except Exception as e:
        print(f"OOOOPS!!!!. Error: {str(e)}")
        missing_uids.add(df['uid'])
    return missing_uids
        

    # try:    
    #     cursor.execute(SQL, (df['target_lang'], df['content'], df['uid'], df['revision'], now, None, engine, metadata, df['target_lang'], field, now, engine, metadata))
    #     #result = cursor.fetchone()
    #     #print(result)
    # except Exception as e:
    #     print(f"OOOOPS!!!!. Error: {str(e)}")
    #     missing_uids.add(df['uid'])
        
    # return missing_uids
    
    #print(cursor.mogrify(SQL, ('en', df['content'], df['uid'], df['revision'], now, None, engine, metadata, 'en', field, now, engine, metadata)))
    # cursor.execute('SELECT EXISTS(SELECT 1 FROM "ContentItem" WHERE "uid" = %s)', (df['uid'],))
    # result = cursor.fetchone()
    # def verify_content_item_exists(uid, cursor):
    #     print('SELECT 1 FROM "ContentItem" WHERE uid = %s', (uid,))
    #     cursor.execute('SELECT 1 FROM "ContentItem" WHERE uid = %s', (uid,))
    #     return cursor.fetchone() is not None
    
    # if verify_content_item_exists(df['uid'], cursor):

    
# Example usage
if __name__ == "__main__":
    # folder_path = "250217_output"
    # # missing_uids_file = "250207_data/missing_uids.txt"
    # # missing_uids_all = set()  # Use a set to avoid duplicates

    # if INSERT_SINGLE_FILE_ONLY:
    #     # SINGLE FILE:
    #     fn = sys.argv[1]
    #     file_path = os.path.join(folder_path, fn)
    #     df_results = parse_jsonl_to_dataframe(file_path)
    #     print(df_results.info())
    #     for index,row in df_results.iterrows():
    #         #load_into_contentItemTranslation(row)
    #         print(row)
    #         missing_uids = load_into_contentItemTranslation(row)
    #         missing_uids_all.update(missing_uids)
    #     print(80*"=")
    #     print(missing_uids_all)   
    #     conn.commit()
    #     cursor.close()

    # else:
    #     # Or Multiple files...
            
    # # # # Read existing missing UIDs from the file (if it exists)
    # # if os.path.exists(missing_uids_file):
    # #     with open(missing_uids_file, "r") as f:
    # #         missing_uids_all.update(f.read().splitlines())  # Load existing UIDs
    # # now insert all...
    #     for file_name in os.listdir(folder_path):
    #         if file_name.endswith(".jsonl"):
    #             file_path = os.path.join(folder_path, file_name)
    #         df_results = parse_jsonl_to_dataframe(file_path)
    #         for index,row in df_results.iterrows():
    #             load_into_contentItemTranslation(row)
    #             # print(row)
    #             # missing_uids = load_into_contentItemTranslation(row)
    #             # missing_uids_all.update(missing_uids)
    #         conn.commit()
    # cursor.close()

    # cursor.execute("""
    #     SELECT 
    #     "targetLanguage", 
    #     COUNT(*) AS input_count
    #     FROM 
    #         "ContentItemTranslation"
    #     GROUP BY 
    #         "targetLanguage"
    #     ORDER BY 
    #         input_count DESC;
    #         """)
    cursor.execute("""
    SELECT count(*) AS cnt, "targetLanguage" AS lang
    FROM "ContentItemTranslation"
    GROUP BY lang
    ORDER BY cnt DESC;
    """)
    result = cursor.fetchall()
    print(result)
    conn.commit()
    cursor.close()
    
        # # Write back all missing UIDs (old + new)
        # if missing_uids_all:
        #     with open(missing_uids_file, "w") as f:
        #         f.write("\n".join(missing_uids_all))
        
        # print(f"Updated missing UIDs saved to {missing_uids_file}")  # Confirmation message
        
                # with open(file_path, 'r', encoding='utf-8') as f:
                #     data = [json.loads(line) for line in f]
        
                # df_results = pd.DataFrame(data)
        
                # for _, row in df_results.iterrows():
                #     load_into_contentItemTranslation(row)
        # # df_results = parse_jsonl_to_dataframe(sys.argv[1])


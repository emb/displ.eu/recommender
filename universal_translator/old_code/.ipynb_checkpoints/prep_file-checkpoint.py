import os
import json
import logging
from collections import defaultdict
from openai import OpenAI
import pandas as pd
import math
from dotenv import load_dotenv
from tokens_count import num_tokens_from_string
# from main import MODEL
from pathlib import Path
from datetime import datetime
load_dotenv()

MODEL = "gpt-4o-mini"
API_KEY = os.getenv("OPENAI_API_KEY")

language_codes = {
        "English": "en",
        "German": "de",
        "Croatian": "hr",
        "Serbian": "sr", # Croatian, Serbian, Bosnian are very similar, probably only need 1
        # "Bosnian": "bs", # Croatian, Serbian, Bosnian are very similar, probably only need 1
        "Czech": "cs",
        "Slovakian": "sk",
        "French": "fr",
        "Hungarian": "hu",
        "Italian": "it",
        "Polish": "pl",
        "Spanish": "es",
        "Dutch": "nl",
        "Portuguese": "pt",
        "Ukrainian": "uk",
        "Russian": "ru",
        "Turkish": "tr",
        "Romanian": "ro",
        "Bulgarian": "bg",
        "Finnish": "fi",
        "Greek": "el",
        "Swedish": "sv"
    }

def create_all_language_examples():
    """
    Processes content items to retrieve three examples for each language code.
    Exports the results to a new JSON file.
    Logs a warning if a language code has fewer than three examples or is not found.
    """
    try:
        with open('250207_data/content_item_excluded_cba.json', 'r', encoding='utf-8') as file:
            data = json.load(file)
    except FileNotFoundError:
        logging.error("The file 250207_data/content_item_excluded_cba.json was not found.")
        return
    except json.JSONDecodeError:
        logging.error("Error decoding JSON from the file.")
        return

    language_map = defaultdict(list)

    # Populate the language_map with items for each language code
    for item in data:
        original_languages = item.get('originalLanguages', {}).get('language_codes', [])
        for lang_code in original_languages:
            language_map[lang_code].append(item)

    all_examples = []

    # Iterate through each language code to retrieve examples
    for lang_code, items in language_map.items():
        if len(items) < 3:
            logging.warning(f"Not enough examples for language code '{lang_code}'. Required: 3, Found: {len(items)}.")

        for item in items[:3]:
            # Optionally, you can add the language code to the item for reference
            item_with_lang = item.copy()
            item_with_lang['language_code'] = lang_code
            all_examples.append(item_with_lang)

    # Export the collected examples to a new JSON file
    try:
        output_file = '250207_data/all_language_examples.json'
        with open(output_file, 'w', encoding='utf-8') as outfile:
            json.dump(all_examples, outfile, ensure_ascii=False, indent=4)
        logging.info(f"Successfully exported language examples to {output_file}.")
    except IOError as e:
        logging.error(f"Failed to write to {output_file}: {e}")
            
            
def create_language_examples(language_code, number_of_examples):
    """
    Processes content items to retrieve up to 'number_of_examples' for the specified language code.
    Exports the results to a new JSON file.
    Logs a warning if fewer than 'number_of_examples' are found.

    Parameters:
    - language_code (str): The language code to filter examples by (e.g., 'en', 'de').
    - number_of_examples (int): The maximum number of examples to retrieve.
    """
    try:
        with open('250207_data/content_item_excluded_cba.json', 'r', encoding='utf-8') as file:
            data = json.load(file)
    except FileNotFoundError:
        logging.error("The file 250207_data/content_item_excluded_cba.json was not found.")
        return
    except json.JSONDecodeError:
        logging.error("Error decoding JSON from the file.")
        return

    examples = []

    # Iterate through each item and collect those with the specified language code
    for item in data:
        original_languages = item.get('originalLanguages', {}).get('language_codes', [])
        if language_code in original_languages:
            item_with_lang = item.copy()
            item_with_lang['language_code'] = language_code
            examples.append(item_with_lang)
            if len(examples) == number_of_examples:
                break

    if len(examples) < number_of_examples:
        logging.warning(f"Not enough examples for language code '{language_code}'. Required: {number_of_examples}, Found: {len(examples)}.")

    # Export the collected examples to a new JSON file
    try:
        output_file = f'250207_data/{language_code}_language_examples_{number_of_examples}.json'
        with open(output_file, 'w', encoding='utf-8') as outfile:
            json.dump(examples, outfile, ensure_ascii=False, indent=4)
        logging.info(f"Successfully exported {[k for k, v in language_codes.items() if v == language_code][0]} language examples to {output_file}.")
    except IOError as e:
        logging.error(f"Failed to write to {output_file}: {e}")
    return examples
  

def extract_and_export_input_data(input_file, output_file: str="input_jsonl_transformed.json"):
    data = []
    # Read the JSONL file
    with open(input_file, 'r', encoding='utf-8') as f:
        for line in f:
            data.append(json.loads(line))

    input_data = []
    for entry in data:
        custom_id = entry.get("custom_id")
        input_data.append({
            "custom_id": custom_id,
            "content": entry['body']['messages'][1]['content']
        })
    print("Input data length:", len(input_data))
    
    # Exporting the input data to another JSON file
    with open(output_file, 'w') as outfile:
        json.dump(input_data, outfile, indent=4)
    print("Exported to", output_file)
    
    return input_data

    
def extract_and_export_output_data(input_file, output_file: str="output_jsonl_transformed.json"):
    data = []
    # Read the JSONL file
    with open(input_file, 'r', encoding='utf-8') as f:
        for line in f:
            data.append(json.loads(line))

    output_data = []

    for entry in data:
        custom_id = entry.get("custom_id")
        choices = entry.get("response", {}).get("body", {}).get("choices", [])

        for choice in choices:
            content = choice.get("message", {}).get("content")

            output_data.append({
                "custom_id": custom_id,
                "content": content,
            })
            
    print("Output data length:", len(output_data))
    
    # Exporting the output data to another JSON file
    with open(output_file, 'w') as outfile:
        json.dump(output_data, outfile, indent=4)
    print("Exported to", output_file)
    
    return output_data
    
    
def create_inspect_file(input_file, output_file):
    with open(input_file, 'r', encoding='utf-8') as f:
        data = json.load(f)
        
    inspect_data = []
  
    def create_inspect_item(uid, revisionid, field, content, language_code):
                    return {
                            "custom_id": f"{uid}_{revisionid}_{field}_{language_code}",
                            "content": content
                    }
    
    for entry in data:
            uid = entry.get("uid")
            revisionid = entry.get("revisionId")
            original_langs = entry.get("originalLanguages", {})
            language_codes = original_langs.get("language_codes", ["en"])
            unique_language_codes = set(language_codes)		    
            
            # Process each language in language_codes
            for language_code in unique_language_codes:
                    fields = ["title", "summary", "content"]
                    for field in fields:
                            field_data = entry.get(field, {})
                            content_dict = field_data.get(language_code, {})
                            field_content = content_dict.get("value", "").strip()

                            if field_content:
                                    inspect_item = create_inspect_item(uid, revisionid, field, field_content, language_code)
                                    inspect_data.append(inspect_item)
                                    
    with open(output_file, 'w', encoding='utf-8') as outfile:
        json.dump(inspect_data, outfile, indent=4)
        
    print("Exported to", output_file)


def create_evaluation_xls_file(output_xls_path, input_json_path, output_json_path):
    """
    Creates an evaluation Excel file by merging original and translated texts.

    Parameters:
    - output_xls_path (str): Path to save the resulting Excel file.
    - original_json_path (str): Path to the JSON file containing original texts.
    - translated_json_path (str): Path to the JSON file containing translated texts.
    """
    # Check if original JSON file exists
    if not os.path.isfile(input_json_path):
        print(f"Original JSON file not found at {input_json_path}")
        return

    # Check if translated JSON file exists
    if not os.path.isfile(output_json_path):
        print(f"Translated JSON file not found at {output_json_path}")
        return

    # Load original texts
    try:
        with open(input_json_path, 'r', encoding='utf-8') as f:
            input_data = json.load(f)
    except json.JSONDecodeError:
        print(f"Error decoding JSON from {input_json_path}")
        return

    # Load translated texts
    try:
        with open(output_json_path, 'r', encoding='utf-8') as f:
            output_data = json.load(f)
    except json.JSONDecodeError:
        print(f"Error decoding JSON from {output_json_path}")
        return

    # Create dictionaries for quick lookup
    input_dict = {}
    for item in input_data:
        custom_id = item.get('custom_id', '')
        parts = custom_id.split('_')
        if len(parts) != 4:
            print(f"Original custom_id has unexpected format: {custom_id}")
            continue
        key = custom_id  # "{uid}_{revisionid}_{field}_{language_code}"
        input_dict[key] = item.get('content', '')

    output_dict = {}
    for item in output_data:
        custom_id = item.get('custom_id', '')
        parts = custom_id.split('_')
        # if len(parts) < 5:
        #     print(f"Translated custom_id has unexpected format: {custom_id}")
        #     continue
        # Extract the first four parts to match the original custom_id
        key = '_'.join(parts[:4])  # "{uid}_{revisionid}_{field}_{language_code}"
        output_dict[key] = item.get('content', '')

    # Find common keys
    common_keys = sorted(set(input_dict.keys()) & set(output_dict.keys()))

    if not common_keys:
        print("No matching custom_ids found between original and translated data.")
        return

    # reviewers = ['Kay', 'Aaron', 'Ingo', 'Gerald', 'Michael']
    reviewers = ['Kay']
    num_reviewers = len(reviewers)
    total_items = len(common_keys)
    items_per_reviewer = math.ceil(total_items / num_reviewers)

    rows = []

    for idx, key in enumerate(common_keys):
        uid, revId, field_name, language_code = key.split('_')
        input_text = input_dict.get(key, "")
        output_text = output_dict.get(key, "")

        # Assign reviewer based on chunk
        reviewer_index = idx // items_per_reviewer
        if reviewer_index >= num_reviewers:
            reviewer_index = num_reviewers - 1  # Assign to last reviewer if overflow
        reviewer = reviewers[reviewer_index]

        row = {
            'field name': field_name,
            'uid': uid,
            'revId': revId,
            # 'original_text': original_text,
            # 'translated_text': translated_text,
            'input_text':  input_text,
            'output_text': output_text,
            'score (0-5)': '',
            'comment \n why you scored it like that': '',
            'who scored it?': reviewer
        }

        rows.append(row)

    # Create DataFrame
    df = pd.DataFrame(rows, columns=[
        'field name',
        'uid',
        'revId',
        # 'original_text',
        # 'translated_text',
        'input_text',
        'output_text',
        'score (0-5)',
        'comment \n why you scored it like that',
        'who scored it?'
    ])

    # Export to Excel
    try:
        df.to_excel(output_xls_path, index=False, engine='openpyxl')
        print(f"Evaluation Excel file created successfully at {output_xls_path}")
    except Exception as e:
        print(f"Failed to write Excel file: {e}")
                

def create_language_examples(language_code, number_of_examples, model, max_tokens=16000):
    """
    Processes content items to retrieve up to 'number_of_examples' for the specified language code,
    ensuring each example's content exceeds the maximum token limit.
    Exports the results to a new JSON file.
    Logs a warning if fewer than 'number_of_examples' are found or if token limits are not met.

    Parameters:
    - language_code (str): The language code to filter examples by (e.g., 'en', 'de').
    - number_of_examples (int): The maximum number of examples to retrieve.
    - max_tokens (int): The minimum number of tokens required for each example.

    Returns:
        None
    """
    try:
        with open('250207_data/content_item_excluded_cba.json', 'r', encoding='utf-8') as file:
            data = json.load(file)
    except FileNotFoundError:
        logging.error("The file 250207_data/content_item_excluded_cba.json was not found.")
        return
    except json.JSONDecodeError:
        logging.error("Error decoding JSON from the file.")
        return

    language_examples = []

    # Iterate through each item and collect those with the specified language code and token count
    for item in data:
        original_languages = item.get('originalLanguages', {}).get('language_codes', [])
        if language_code in original_languages:
            content = item.get('content', {}).get(language_code, {}).get('value', "").strip()
            if content:
                token_count = num_tokens_from_string(content, model)  # Replace 'your-model-name' accordingly
                if token_count > max_tokens:
                    # Keep the existing code snippet
                    item_with_lang = item.copy()
                    item_with_lang['language_code'] = language_code
                    language_examples.append(item_with_lang)
                    
                    if len(language_examples) == number_of_examples:
                        break

    if len(language_examples) < number_of_examples:
        logging.warning(
            f"Not enough examples for language code '{language_code}' meeting the token requirement."
            f" Required: {number_of_examples}, Found: {len(language_examples)}."
        )

    # Export the collected examples to a new JSON file
    try:
        output_file = f'250207_data/{language_code}_language_examples_{number_of_examples}.json'
        with open(output_file, 'w', encoding='utf-8') as outfile:
            json.dump(language_examples, outfile, ensure_ascii=False, indent=4)
        logging.info(f"Successfully exported {language_code} language examples to {output_file}.")
    except IOError as e:
        logging.error(f"Failed to write to {output_file}: {e}")
        
    return language_examples


def log_batch_job(input_file, batch_job_id, tracking_file='250207_data/batch_jobs_tracking.json'):
    """
    Logs the input JSONL file and corresponding batch job IDs to a tracking file.

    Args:
        input_file (str): Path to the input JSONL file.
        batch_job_id (str): batch job ID.
        tracking_file (str): Path to the tracking file.
    """
    tracking_data = {}
    tracking_path = Path(tracking_file)
    # Load existing tracking data if the file exists
    if tracking_path.exists():
        with open(tracking_path, 'r', encoding='utf-8') as f:
            try:
                tracking_data = json.load(f)
            except json.JSONDecodeError:
                tracking_data = {}
    
    # # Create a unique key for the current batch
    # key = f"{input_file}_{len(batch_job_id)}"
    
    # Update tracking data
    tracking_data[input_file] = {
        "input_file": input_file,
        "batch_job_id": batch_job_id,
        "status": "pending"
    }
    
    # Write back to the tracking file
    with open(tracking_path, 'w', encoding='utf-8') as f:
        json.dump(tracking_data, f, ensure_ascii=False, indent=4)


def process_batch_jobs(tracking_file='250207_data/batch_jobs_tracking.json'):
    """
    Processes all batch jobs by retrieving output files, extracting and exporting data,
    creating evaluation Excel files, and handling errors by logging them.

    Parameters:
    - tracking_file (str): Path to the tracking JSON file.

    Returns:
    - None
    """
    # Initialize OpenAI client
    client = OpenAI(api_key=API_KEY)
    
    # Load tracking data
    try:
        with open(tracking_file, 'r', encoding='utf-8') as f:
            tracking_data = json.load(f)
    except FileNotFoundError:
        logging.error(f"Tracking file {tracking_file} not found.")
        return
    except json.JSONDecodeError:
        logging.error(f"Error decoding JSON from {tracking_file}.")
        return
    
    # Iterate over each input file and its associated batch_job_id
    for input_file, details in tracking_data.items():
        status = details.get("status", "pending")
        if status == "processed":
            logging.info(f"Skipping already processed file: {input_file}")
            continue  # Skip already processed files
        
        print('START PROCESSING', input_file)
        batch_job_id = details.get('batch_job_id', [])
        if not batch_job_id:
            logging.warning(f"No batch_job_id found for {input_file}.")
            continue
        
        error_records = []  # To store error information
        
        # Retrieve batch job details
        batch_job = client.batches.retrieve(batch_job_id)
        output_file_id = batch_job.output_file_id
        error_file_id = batch_job.error_file_id  # Safely get error_file_id
            
        if output_file_id:
            # Fetch the output file
            output_file_content = client.files.content(output_file_id)
            if not output_file_content:
                logging.error(f"Failed to fetch output content for batch_job_id {batch_job_id}.")
                continue
                    
            output_file = f"output/{batch_job_id}_output.jsonl"
            
            output_lines = output_file_content.text.splitlines()
            with open(output_file, 'w', encoding='utf-8') as outfile:
                for line in output_lines:
                    outfile.write(line + '\n')
            
            # input_json_file = input_file.rstrip('.jsonl') + '_input.json'
            output_json_file = output_file.rstrip('.jsonl') + '_output.json'
            
            # Process the input and output files
            # extract_and_export_input_data(input_file, input_json_file)
            extract_and_export_output_data(output_file, output_json_file)
            
            # # Create evaluation Excel file
            # create_evaluation_xls_file(
            #     output_xls_path=f"250207_data/evaluation_{batch_job_id}.xlsx",
            #     input_json_path=input_json_file,
            #     output_json_path=output_json_file
            # )
            
            logging.info(f"Processed batch_job_id {batch_job_id} successfully.")
            # Mark the input_file as processed
            tracking_data[input_file]["status"] = "processed"
        
        # Handle error_file_id if present
        if error_file_id:
            error_content = client.files.content(error_file_id)
            if error_content:
                error_lines = error_content.text.splitlines()
                for error_line in error_lines:
                    error_dict = json.loads(error_line)
                    if error_dict.get("error", {}):
                        error_records.append({
                            "custom_id": error_dict.get("custom_id", {}),
                            "error_message": error_dict.get("error", {}).get("message", "")
                        })
                    else:
                        error_records.append({
                            "custom_id": error_dict.get("custom_id", {}),
                            "error_message": ""
                        })
                # if custom_id and error_message:
                #     error_record = {
                #         "custom_id": custom_id,
                #         "error_message": error_message
                #     }
                #     error_records.append(error_record)
                #     logging.error(f"Batch_job_id {batch_job_id} encountered an error: {error_message}")
                # else:
                #     logging.error(f"Error data for batch_job_id {batch_job_id} is incomplete.")
            # else:
            #     logging.error(f"Failed to fetch error content for batch_job_id {batch_job_id}.")

        
        # Save error records if any
        if error_records:
            error_file = f"output/{batch_job_id}_error.json"
            # error_output_file = f"250207_data/errors_{os.path.basename(input_file).rstrip('.jsonl')}.json"
            with open(error_file, 'w', encoding='utf-8') as error_file:
                json.dump(error_records, error_file, ensure_ascii=False, indent=4)
            logging.info(f"Logged errors to {error_file}.")
        
        # Update the tracking file
        try:
            with open(tracking_file, 'w', encoding='utf-8') as f:
                json.dump(tracking_data, f, ensure_ascii=False, indent=4)
            logging.info(f"Updated tracking file {tracking_file}.")
        except IOError as e:
            logging.error(f"Failed to update tracking file {tracking_file}: {e}")
        
        print('---------------------------------------------')

def retry_pending_batch_jobs(tracking_file='250207_data/batch_jobs_tracking.json'):
    """
    Reads the tracking file and retries any batch jobs with status 'pending'.
    Updates the tracking file with new batch_job_id after resubmission.
    
    Args:
        tracking_file_path (str): Path to the tracking JSON file.
        api_key (str): OpenAI API key.
    """
    with open(tracking_file, 'r', encoding='utf-8') as file:
        tracking_data = json.load(file)
    
    client = OpenAI(api_key=API_KEY)
    updated = False

    for key, value in tracking_data.items():
        if value.get("status") == "pending":
            input_file = value.get("input_file")
            if not input_file or not os.path.exists(input_file):
                print(f"Input file {input_file} does not exist. Skipping.")
                continue
            batch_job_id = value.get("batch_job_id")
            saved_batch = client.batches.retrieve(batch_job_id)
            if saved_batch.status == 'failed':
                try:
                    # Resubmit the batch job
                    batch_input_file = client.files.create(
                        file=open(input_file, "rb"),
                        purpose="batch"
                    )
                    batch_input_file_id = batch_input_file.id
    
                    batch_job = client.batches.create(
                        input_file_id=batch_input_file_id,
                        endpoint="/v1/chat/completions",
                        completion_window="24h",
                        metadata={
                            "description": f"Retry batch job for {input_file}"
                        }
                    )
                    print(f"Resubmitted batch job for {input_file}: {batch_job.id}")
                    
                    # Update the tracking data
                    tracking_data[key]["batch_job_id"] = batch_job.id
                    tracking_data[key]["status"] = "processed"
                    updated = True
                    
                except Exception as e:
                    print(f"Failed to resubmit batch job for {input_file}: {e}")
    
    if updated:
        # Overwrite the original tracking file with updated data
        with open(tracking_file, 'w', encoding='utf-8') as file:
            json.dump(tracking_data, file, ensure_ascii=False, indent=4)
        print(f"Tracking file {tracking_file} has been updated with new batch_job_id.")
    else:
        print("No pending batch jobs found to retry.")

def remove_duplicates(input_file, output_file):
    """
    Removes duplicate dictionaries from a JSON file based on the 'uid' field.

    :param input_file: Path to the input JSON file containing a list of dictionaries.
    :param output_file: Path to the output JSON file to save unique dictionaries.
    """
    try:
        with open(input_file, 'r', encoding='utf-8') as f:
            data = json.load(f)
        
        unique_data = []
        seen_uids = set()

        i = 0
        for entry in data:
            uid = entry.get('uid')
            if uid and uid not in seen_uids:
                unique_data.append(entry)
                seen_uids.add(uid)
            else:
                print(f"Duplicate found and removed: UID {uid}")
                i =+ 1

        with open(output_file, 'w', encoding='utf-8') as f:
            json.dump(unique_data, f, indent=4, ensure_ascii=False)
        
        print(f"{i} duplicates removed. Unique data saved to '{output_file}'.")
    
    except FileNotFoundError:
        print(f"Error: The file '{input_file}' does not exist.")
    except json.JSONDecodeError:
        print(f"Error: The file '{input_file}' is not valid JSON.")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")


def process_error_jobs(tracking_file='250207_data/batch_jobs_tracking.json', output_folder='output', jsonl_output_folder='250207_data'):
    # Load batch job tracking file
    with open(tracking_file, 'r', encoding='utf-8') as f:
        batch_jobs = json.load(f)
    
    error_items = []
    
    # Process each batch job
    for job_data in batch_jobs.values():
        batch_job_id = job_data.get('batch_job_id')
        input_file = job_data['input_file']
        
        if not batch_job_id:
            continue

        # Construct error file path
        error_file = os.path.join(output_folder, f'{batch_job_id}_error.json')
        
        # Check if error file exists
        if not os.path.exists(error_file):
            continue
            
        # Load error file
        with open(error_file, 'r', encoding='utf-8') as f:
            error_data = json.load(f)
        
        # Load input file
        with open(input_file, 'r', encoding='utf-8') as f:
            input_lines = [json.loads(line) for line in f]
        
        # Create lookup for input items
        input_lookup = {item['custom_id']: item for item in input_lines}
        
        # Find matching items
        for error_item in error_data:
            # Extract custom_id from error item (handles both string and dict formats)
            custom_id = error_item if isinstance(error_item, str) else error_item.get('custom_id')
            if custom_id and custom_id in input_lookup:
                error_items.append(input_lookup[custom_id])
    
    # Export all error items to JSONL file
    current_date = datetime.now().strftime('%Y%m%d')
    output_file = os.path.join(jsonl_output_folder, f'all_errors_{current_date}.jsonl')
    with open(output_file, 'w', encoding='utf-8') as f:
        for item in error_items:
            f.write(json.dumps(item) + '\n')
    
    print(f"Exported {len(error_items)} error items to {output_file}")

def remove_duplicates_jsonl(input_file, output_file):
    seen_ids = set()
    unique_entries = []

    with open(input_file, 'r', encoding='utf-8') as f:
        for line in f:
            entry = json.loads(line)
            custom_id = entry.get('custom_id')
            
            if custom_id not in seen_ids:
                seen_ids.add(custom_id)
                unique_entries.append(entry)
    print('Export', len(seen_ids), 'unique items') 

    with open(output_file, 'w', encoding='utf-8') as f:
        for entry in unique_entries:
            f.write(json.dumps(entry, ensure_ascii=False) + '\n')

if __name__ == "__main__":
    # Example usage process_batch_jobs function, may run daily to scrap all batch jobs
    # process_batch_jobs()
    
    # Example usage process_error_jobs function, may run daily to scrap all errors in batch jobs
    # process_error_jobs()

    remove_duplicates_jsonl('250207_data/all_errors_20250217.jsonl', '250207_data/all_errors_20250217_unique.jsonl')
    
#     # # Example usage create_language_examples function
#     # create_language_examples('en', 10, MODEL)

#     # # Example usage create_all_language_examples function
#     # create_all_language_examples()
    
#     # # Example usage create_inspect_file function
#     # create_inspect_file('250207_data/de_language_examples.json', '250207_data/20250205_inspect_de_language_examples.json')
    
#     # # Example usage extract_and_export_data function
#     # input_file = '250207_data/batch_67a230f4c6f0819092364cfe3ccbf501_output.jsonl'
#     # extract_and_export_data(input_file, input_file[:-1])
    
#     # # Example usage create_evaluation_xls_file function
#     # create_evaluation_xls_file('250207_data/20250205_de_to_en_evaluation.xlsx',
#     #                             '250207_data/20250205_inspect_de_language_examples.json',
#     #                             '250207_data/batch_67a230f4c6f0819092364cfe3ccbf501_output.json')    

    # retry_pending_batch_jobs()
    # remove_duplicates('250207_data/250211_latest_content_item_excluded_cba.json', '250207_data/250211_latest_content_item_excluded_cba_unique.json')
SELECT 
public."Revision".uid,
public."Revision"."dateCreated",
public."Revision"."dateModified",
public."ContentItemTranslation"."targetLanguage",
public."ContentItem".uid,
public."ContentItemTranslation"."contentItemUid"


FROM 
public."Revision",
public."ContentItem" 
LEFT JOIN public."ContentItemTranslation" ON (
	public."ContentItemTranslation"."contentItemUid" = public."ContentItem".uid
)
WHERE 
public."Revision".uid = public."ContentItem".uid
AND 
-- Filter created translations
--public."ContentItemTranslation"."contentItemUid" is NOT NULL
--- filter for items missing their translations
public."ContentItemTranslation"."contentItemUid" is NULL
AND 
public."Revision"."dateCreated" > '2025-01-21 16:45:29'

--------------
--optional here are more Filters possible:
AND public."Revision"."entityType" = 'ContentItem'
--------------

ORDER BY
--id DESC, 
public."Revision"."dateModified" DESC

--LIMIT 100

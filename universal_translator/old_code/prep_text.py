import re
import html
from markdownify import markdownify as md
from bs4 import BeautifulSoup

class TextPreprocessor:
    """
    A class to preprocess text by removing HTML tags, unescaping HTML entities,
    removing emojis, and normalizing whitespace.
    """
    def __init__(self):
        # Define emoji pattern
        self.emoji_pattern = re.compile(
            "[" 
            "\U0001F600-\U0001F64F"  # Emoticons
            "\U0001F300-\U0001F5FF"  # Symbols & Pictographs
            "\U0001F680-\U0001F6FF"  # Transport & Map Symbols
            "\U0001F1E0-\U0001F1FF"  # Flags
            "\U00002700-\U000027BF"  # Dingbats
            "\U0001F900-\U0001F9FF"  # Supplemental Symbols and Pictographs
            "]+", 
            flags=re.UNICODE
        )
        # self.hf_embeddings = OpenAIEmbeddings()
        # self.text_splitter = SemanticChunker(self.hf_embeddings)

    def preprocess(self, text: str) -> str:
        """
        Preprocess the input text by cleaning HTML, removing emojis, and normalizing whitespace.

        Args:
            text (str): The input text to preprocess.

        Returns:
            str: The preprocessed, clean text.
        """
        # Remove HTML tags using BeautifulSoup
        # soup = BeautifulSoup(text, "html.parser")
        # clean_text = soup.get_text(separator=' ')

        # # ... OR....
        # # convert HTML to markdown, ignore unknown HTML tags which have no representation in markdown
        # # ... XXX implement XXX...
        
        # # Unescape HTML entities
        # clean_text = html.unescape(clean_text)
        clean_text = md(text)
        
        # # Remove some special characters
        # clean_text = re.sub(r'[_\-*]{2,}', '', clean_text)
        # clean_text = re.sub(r'_+', '', clean_text)

        # Remove emojis
        # or maybe leave them? not sure...
        # clean_text = self.emoji_pattern.sub(r'', clean_text)
        
        # Remove special characters except basic punctuation. Note this also removes "http://..." to "http..."
        # clean_text = re.sub(r'[^\w\s.,!?-]', '', clean_text)

        # Normalize whitespace
        clean_text = re.sub(r'\s+', ' ', clean_text).strip()

        return clean_text

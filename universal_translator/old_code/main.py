import json
import warnings
import requests
import os
from openai import OpenAI
from dotenv import load_dotenv
import click
from pathlib import Path
from tokens_count import num_tokens_from_string
from prep_prompt import COMPLEX_TEMPLATE_KEEP_HTML_TRANSLATE
# from prep_file import log_batch_job
warnings.filterwarnings("ignore")

# Load environment variables from .env file
load_dotenv()

API_KEY = os.getenv('OPENAI_API_KEY')

MODEL = "gpt-4o-mini"
MAX_TOKEN = 16000

costs_per_token_in = 0.150   # USD per million tokens
costs_per_token_out = 0.600  # USD per million

INPUT_JSONL_FILE = '250207_data/all_errors_20250217_unique.jsonl'
INPUT_SPLIT_JSONL_FILE_PREFIX = '250207_data/all_errors_20250217_split'

# INPUT_JSON_FILE = '250207_data/250211_latest_content_item_excluded_cba_unique.json'
# INPUT_JSONL_FILE = '250207_data/250211_latest_content_item_excluded_cba_sr_unique.jsonl'
# INPUT_SPLIT_JSONL_FILE_PREFIX = '250207_data/250211_latest_content_item_excluded_cba_sr_unique_split'

# INPUT_JSON_FILE = '250207_data/content_item_excluded_cba.json'
# INPUT_JSONL_FILE = '250207_data/content_item_excluded_cba_sr.jsonl'
# INPUT_SPLIT_JSONL_FILE_PREFIX = '250207_data/content_item_excluded_cba_sr_split'

# INPUT_JSON_FILE = 'data/all_language_examples.json'
# INPUT_JSONL_FILE = 'data/all_language_examples.jsonl'
# INPUT_SPLIT_JSONL_FILE_PREFIX = 'data/all_language_examples_split'

# INPUT_JSON_FILE = 'data/test_batch_run.json'
# INPUT_JSONL_FILE = 'data/test_batch_run.jsonl'
# INPUT_SPLIT_JSONL_FILE_PREFIX = 'data/test_batch_run_split'

# INPUT_JSON_FILE = 'data/content_item_excluded_cba-SINGLE_SHOT.json'
# INPUT_JSONL_FILE = 'data/content_item_excluded_cba-SINGLE_SHOT.jsonl'
# INPUT_SPLIT_JSONL_FILE_PREFIX = 'data/content_item_excluded_cba-SINGLE_SHOT_split'


def log_batch_job(input_file, batch_job_id, tracking_file='250207_data/batch_jobs_tracking.json'):
    """
    Logs the input JSONL file and corresponding batch job IDs to a tracking file.

    Args:
        input_file (str): Path to the input JSONL file.
        batch_job_id (str): batch job ID.
        tracking_file (str): Path to the tracking file.
    """
    tracking_data = {}
    tracking_path = Path(tracking_file)
    # Load existing tracking data if the file exists
    if tracking_path.exists():
        with open(tracking_path, 'r', encoding='utf-8') as f:
            try:
                tracking_data = json.load(f)
            except json.JSONDecodeError:
                tracking_data = {}
    
    # # Create a unique key for the current batch
    # key = f"{input_file}_{len(batch_job_ids)}"
    
    # Update tracking data
    tracking_data[input_file] = {
        "input_file": input_file,
        "batch_job_id": batch_job_id,
        "status": "pending"
    }
    
    # Write back to the tracking file
    with open(tracking_path, 'w', encoding='utf-8') as f:
        json.dump(tracking_data, f, ensure_ascii=False, indent=4)


def get_language_codes():
    """
    Returns a dictionary mapping language names to their ISO 639-1 codes.
    """
    language_codes = {
        "English": "en",
        "German": "de",
        "Croatian": "hr",
        "Serbian": "sr", # Croatian, Serbian, Bosnian are very similar, probably only need 1
        # "Bosnian": "bs", # Croatian, Serbian, Bosnian are very similar, probably only need 1
        "Czech": "cs",
        "Slovakian": "sk",
        "French": "fr",
        "Hungarian": "hu",
        "Italian": "it",
        "Polish": "pl",
        "Spanish": "es",
        "Dutch": "nl",
        "Portuguese": "pt",
        "Ukrainian": "uk",
        "Russian": "ru",
        "Turkish": "tr",
        "Romanian": "ro",
        "Bulgarian": "bg",
        "Finnish": "fi",
        "Greek": "el",
        "Swedish": "sv"
    }
    return language_codes

# Function to generate the custom JSON format
def generate_custom_json(data, model, template, translated_language_name, translated_language_code):
		total_token = 0
		custom_data = []
		# inspect_data = []
  
		def create_item(uid, revisionid, field, content, translating_language_code, model, template, translated_language_name, translated_language_code):
						return {
								"custom_id": f"{uid}_{revisionid}_{field}_{translating_language_code}_{translated_language_code}",
								"method": "POST",
								"url": "/v1/chat/completions",
								"body": {
										"model": model,
										"messages": [
												{"role": "system", "content": template.format(TARGET_LANGUAGE=translated_language_name)},
												{"role": "user", "content": content}
										], 
										"max_tokens": MAX_TOKEN,
										"temperature": 0,
										"top_p": 1,
										"frequency_penalty": 0,
										"presence_penalty": 0,
										"seed": 42
								}
						}
		
		for entry in data:
				uid = entry.get("uid")
				revisionid = entry.get("revisionId")
				original_langs = entry.get("originalLanguages", {})
				language_codes = original_langs.get("language_codes", ["en"])
				unique_language_codes = set(language_codes)		    
				
				# Process each language in language_codes
				for language_code in unique_language_codes:
						fields = ["title", "summary", "content"]
						for field in fields:
								field_data = entry.get(field, {})
								content_dict = field_data.get(language_code, {})
								field_content = content_dict.get("value", "").strip()

								if field_content:
										field_token = num_tokens_from_string(field_content, model)
										total_token += min(field_token, MAX_TOKEN)
										custom_item = create_item(uid, revisionid, field, field_content, language_code, model, template, translated_language_name, translated_language_code)
										custom_data.append(custom_item)
		return custom_data, total_token

# Send realtime request to OpenAI
def send_realtime_request_to_openai(data, api_key):
    endpoint = "https://api.openai.com/v1/chat/completions"
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {api_key}"
    }
    try:
        response = requests.post(endpoint, headers=headers, json=data["body"])
        response.raise_for_status()
        return response.json()
    except requests.exceptions.HTTPError as http_err:
        
        print(f"HTTP error occurred: {http_err} - Response: {response.text}")
    except Exception as err:
        
        print(f"An error occurred: {err}")
        
# Function to split a JSONL file into smaller parts with a maximum of 25000 items each
def split_jsonl_file(data, output_prefix, max_items=25000):
    split_files = []
    current_split = 1
    current_count = 0
    split_filename = f"{output_prefix}_part{current_split}.jsonl"
    split_f = open(split_filename, 'w', encoding='utf-8')
    split_files.append(split_filename)

    for item in data:
        json.dump(item, split_f, ensure_ascii=False)
        split_f.write('\n')
        current_count += 1
        if current_count >= max_items:
            split_f.close()
            current_split += 1
            split_filename = f"{output_prefix}_part{current_split}.jsonl"
            split_f = open(split_filename, 'w', encoding='utf-8')
            split_files.append(split_filename)
            current_count = 0
    split_f.close()
    return split_files

@click.command()
@click.option(
    '--language',
    required=True,
    type=click.Choice(['en', 'all', 'none'], case_sensitive=False),
    help='Language to translate to'
)
@click.option(
    '--run_mode',
    required=True,
    type=click.Choice(['realtime', 'batch', 'none'], case_sensitive=False),
    help='Running modes of OPENAI API: realtime or batch'
)
def main(language: str, run_mode: str):
    """
    Generates JSON data and runs translation based on the specified language and run mode.
    """
    # Generate JSON data
    total_tokens = 0
    output_json_datas = []
    # # Open the JSON file, load and transform data to OPENAI API format
    # with open(INPUT_JSON_FILE, 'r', encoding='utf-8') as json_file:
    #     json_data = json.load(json_file)
    # codes = get_language_codes()
    
    # print('Number of input items:', len(json_data))
    
    if language.lower() == 'all':
        # Open the JSON file, load and transform data to OPENAI API format
        with open(INPUT_JSON_FILE, 'r', encoding='utf-8') as json_file:
            json_data = json.load(json_file)
        codes = get_language_codes()
        
        print('Number of input items:', len(json_data))
        print('Number of translating languages:', len(codes))
        for lang, code in codes.items():
            output_json_data, total_token = generate_custom_json(
                json_data, MODEL, COMPLEX_TEMPLATE_KEEP_HTML_TRANSLATE, lang, code)
            output_json_datas.extend(output_json_data)
            total_tokens += total_token
        with open(INPUT_JSONL_FILE, 'w', encoding='utf-8') as json_file:
            for item in output_json_datas:
                json_file.write(json.dumps(item, ensure_ascii=False) + '\n')
        print(f"Exported to {INPUT_JSONL_FILE}")
        print("TOTAL TOKEN(INPUT):", total_tokens)
        costs = total_tokens * (costs_per_token_in + costs_per_token_out) / (10**6)
        print("COST ESTIMATE: ", costs, "$")
        print("COST ESTIMATE with 50% Batch discount: ", costs / 2.0, "$")

    elif language.lower() == 'en':
        # Open the JSON file, load and transform data to OPENAI API format
        with open(INPUT_JSON_FILE, 'r', encoding='utf-8') as json_file:
            json_data = json.load(json_file)
        codes = get_language_codes()
        
        print('Number of input items:', len(json_data))
        output_json_datas, total_tokens = generate_custom_json(
            json_data, MODEL, COMPLEX_TEMPLATE_KEEP_HTML_TRANSLATE, 'English', 'en')
        with open(INPUT_JSONL_FILE, 'w', encoding='utf-8') as json_file:
            for item in output_json_datas:
                json_file.write(json.dumps(item, ensure_ascii=False) + '\n')
        print(f"Exported to {INPUT_JSONL_FILE}")
        costs = total_tokens * (costs_per_token_in + costs_per_token_out) / (10**6)
        print("COST ESTIMATE: ", costs, "$")
        print("COST ESTIMATE with 50% Batch discount: ", costs / 2.0, "$")

    elif language.lower() == 'none':
        with open(INPUT_JSONL_FILE, 'r', encoding='utf-8') as json_file:
            output_json_datas = [json.loads(line) for line in json_file]
    else:
        print(f"Language code {language} not supported, please use 'en' or 'all'")
        return
    
    # Run Translation based on run_mode
    if run_mode.lower() == 'realtime':
        response_data = []
        for item in output_json_datas:
            # Send realtime request to OpenAI
            response = send_realtime_request_to_openai(item, API_KEY)
            if response:
                response_data.append({
                    "custom_id": item["custom_id"],
                    "content": response["choices"][0]["message"]["content"]
                })
        
        # Optionally, save the response_data to a file
        realtime_output_file = f'250207_data/realtime_response_{language}.json'
        with open(realtime_output_file, 'w', encoding='utf-8') as outfile:
            json.dump(response_data, outfile, ensure_ascii=False, indent=4)
        print(f"Realtime translation results saved to {realtime_output_file}")

    elif run_mode.lower() == 'batch':
        client = OpenAI(api_key=API_KEY)
        max_items_per_file = 25000
        # batch_job_ids = []
        
        if len(output_json_datas) <= max_items_per_file:
            batch_input_file = client.files.create(
                file=open(INPUT_JSONL_FILE, "rb"),
                purpose="batch"
            )
            batch_input_file_id = batch_input_file.id
            batch_job = client.batches.create(
                    input_file_id=batch_input_file_id,
                    endpoint="/v1/chat/completions",
                    completion_window="24h",
                    metadata={
                        "description": "nightly translation job"
                    }
            )
            print(batch_job)
            # batch_job_ids.append(batch_job.id)
            log_batch_job(INPUT_JSONL_FILE, batch_job.id)
        else:

            split_files = split_jsonl_file(output_json_datas, INPUT_SPLIT_JSONL_FILE_PREFIX, max_items=max_items_per_file)
            # Create batch jobs for each split file
            
            for split_file in split_files:
                try:
                    batch_input_file = client.files.create(
                        file=open(split_file, "rb"),
                        purpose="batch"
                    )
                    batch_input_file_id = batch_input_file.id
                    batch_job = client.batches.create(
                        input_file_id=batch_input_file_id,
                        endpoint="/v1/chat/completions",
                        completion_window="24h",
                        metadata={
                            "description": f"Batch job for {split_file}"
                        }
                    )
                    print(f"Created batch job for {split_file}: {batch_job.id}")
                    # batch_job_ids.append(batch_job.id)
                    log_batch_job(split_file, batch_job.id)

                except Exception as e:
                    print(f"Failed to create batch job for {split_file}: {e}")
        
        
    elif run_mode.lower() == 'none':
        pass
    else:
        print(f"Run mode {run_mode} not supported, please use 'realtime', 'batch' or 'none'")
        return


if __name__ == "__main__":
    main()

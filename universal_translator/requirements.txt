tiktoken == 0.8.0
openai == 1.54.3
python-dotenv == 1.0.1 
psycopg == 3.2.3
beautifulsoup4==4.10.0
langchain-experimental == 0.3.3
jupyter==1.1.1
# langchain-openai == 0.2.10
langchain-community == 0.3.8
sentence-transformers == 3.3.1
markdownify == 0.14.1

# README

How do I use this directory/sub-repo?
Where do I start?

# Installation / preparation

This section assumes that you are running the code from a virtual environment.
If you need to run it from within a docker container (to reach the repco DB which is in another docker container), then please skip this section and go to the section "Installation via docker".

## 1. Make a virtual env and install dependencies:

```bash
virtualenv --python=python3.11 venv
source venv/bin/activate
pip install -r requirements.txt
```

## 2. Set up your openai credentials
```bash
vi .env
# ... add a variable OPENAI_API_KEY=sk-....

set -a
source .env
```

## 3. Run jupyter notebook


```bash
jupyter-notebook --ip=0.0.0.0 batch_api_run.ipynb
```


# Installation via docker

The following is a skeleton docker-compose.yml snippet which shall help you to run the jupyter nb from within docker **so that it has access to the right repco DB**.

```yaml
  jupyter:
    image: quay.io/jupyter/base-notebook
    container_name:  jupyter
    # the following env file is crucial to set the right env vars for accessing the right repco DB + openai API key:
    env_file: /home/aaron/git/recommender/.env
    ports:
      - "8889:8888"
    dns: 8.8.8.8
    volumes:
      # this mount allows us to "see" the directory inside of the docker jupyter nb container
      - "/home/aaron/git/recommender/gpt_batch_translate/:/home/jovyan/work"
    command: start-notebook.py --NotebookApp.token='<SOME RANDOM TOKEN STRING>'

```

**Note well**: this snippet assumes it is inside of one big docker-compose.yml file which allows it to access the repco database docker container by hostname implicilty (since every container is on the same docker network).

## .env file

```
# key associated with the aaronkaplan organisation , used ONLY for display2. 
OPENAI_API_KEY=sk-proj-....

# Postgresql DB credentials
DB_NAME=repco-live-21.1
DB_USER=repco
DB_PASSWORD=<REPCO_PASSWORD>
DB_HOST=repco-db
DB_PORT=5432

```


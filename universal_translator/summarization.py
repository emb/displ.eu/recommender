import json
import os
# from prep_file import create_language_examples
# from tokens_count import num_tokens_from_string
# from main import OpenAI, API_KEY, split_jsonl_file
from openai import OpenAI
from dotenv import load_dotenv
from pathlib import Path
import re
import string
from bs4 import BeautifulSoup
import tiktoken

# Load environment variables from .env file
load_dotenv()

API_KEY = os.getenv('OPENAI_API_KEY')

MODEL = "gpt-4o-mini"
MAX_TOKEN = 16000

LANGUAGE_CODES = {
    "English": "en",
    "German": "de",
    "Croatian": "hr",
    "Serbian": "sr",
    # "Bosnian": "bs", # Croatian and Bosnian are very similar, so only run Croatian
    "Czech": "cs",
    "Slovakian": "sk",
    "French": "fr",
    "Hungarian": "hu",
    "Italian": "it",
    "Polish": "pl",
    "Spanish": "es",
    "Dutch": "nl",
    "Portuguese": "pt",
    "Ukrainian": "uk",
    "Russian": "ru",
    "Turkish": "tr",
    "Romanian": "ro",
    "Bulgarian": "bg",
    "Finnish": "fi",
    "Greek": "el",
    "Swedish": "sv"
    }

SHORT_INTRO_PROMPT = """
"You are an expert journalist for a major global news publisher, highly skilled at distilling news articles into concise, engaging bulletins. Your task is to read the full article and write a compelling teaser of no more than 50 words. This teaser will appear in a news reader app, so it needs to be clear and engaging for an international audience. Write the teaser in **{TARGET_LANGUAGE}**. Avoid adding information that is not in the article. Avoid spoilers. Ensure readability and encourage users to click on the link to find out more."
"""

LONG_INTRO_PROMPT = """
You are an expert journalist for a leading global news publisher, skilled at distilling articles into concise, engaging summaries. Your task is to:
Read the provided article and distil it into the three most critical points, with the most important point first. Focus on the most intriguing questions, challenges, and issues raised while avoiding spoilers or excessive details. Be clear and concise.
Craft a teaser: Write a 100-word, factual, BBC-style, compelling teaser that captures readers' attention. Ensure it sparks curiosity without giving away too much information. The teaser should prompt the reader to explore the full article.
Write the teaser in **{TARGET_LANGUAGE}**.
"""

def num_tokens_from_string(string: str, encoding_name: str) -> int:
    """Returns number of tokens a string would contain."""

    encoding = tiktoken.encoding_for_model(encoding_name)
    num_tokens = len(encoding.encode(string))
    return num_tokens


def clean_text(text):
    """Removes HTML tags and special characters, keeping only plain text."""
    # Remove HTML tags
    text = BeautifulSoup(text, "html.parser").get_text()
    # Remove non-word characters (except spaces)
    text = re.sub(r'[^\w\s]', '', text)
    # Normalize spaces
    text = re.sub(r'\s+', ' ', text).strip()
    return text


def count_words(text):
    """Counts words in cleaned text."""
    cleaned_text = clean_text(text)
    words = cleaned_text.split()
    return len(words)


# Function to generate the custom JSON format
def generate_content_json(data, model, template, prompt_name):
    total_token = 0
    custom_data = []
    not_summarized_data = []

    def create_item(uid, revisionid, field, content, language_code, model, template):
        language_name = next((name for name, code in LANGUAGE_CODES.items() if code == language_code), None)
        return {
            "custom_id": f"{uid}_{revisionid}_{field}_{language_code}",
            "method": "POST",
            "url": "/v1/chat/completions",
            "body": {
                "model": model,
                "messages": [
                    {"role": "system", "content": template.format(TARGET_LANGUAGE=language_name)},
                    {"role": "user",
                     "content": content}
                ],
                "max_tokens": MAX_TOKEN,
                "temperature": 0,
                "top_p": 1,
                "frequency_penalty": 0,
                "presence_penalty": 0,
                "seed": 42
            }
        }

    max_count = 0
    if template == SHORT_INTRO_PROMPT:
        max_count = 50
    elif template == LONG_INTRO_PROMPT:
        max_count = 100

    for entry in data:
        uid = entry.get("uid")
        revisionid = entry.get("revisionId")
        original_langs = entry.get("originalLanguages", {})
        language_codes = original_langs.get("language_codes", ["en"])
        unique_language_codes = set(language_codes)

        # Process each language in language_codes
        for language_code in unique_language_codes:
            # fields = ["title", "summary", "content"]
            # for field in fields:
            field_data = entry.get("content", {})
            content_dict = field_data.get(language_code, {})
            field_content = content_dict.get("value", "").strip()
            content_word_count = count_words(field_content)
            if field_content:
                if content_word_count >= max_count:
                    field_token = num_tokens_from_string(field_content, model)
                    total_token += min(field_token, MAX_TOKEN)
                    custom_item = create_item(
                        uid, revisionid, "content", field_content, language_code, model, template)
                    custom_data.append(custom_item)
                else:
                    not_summarized_data.append({
                        "custom_id": f"{uid}_{revisionid}_content_{language_code}",
                        "content": field_content
                    })
    not_summarized_file = f"data/teaser/no_summarization_{prompt_name.upper()}.json"
    with open(not_summarized_file, "w", encoding="utf-8") as file:
        json.dump(not_summarized_data, file, indent=4)
    return custom_data, total_token


# Function to split a JSONL file into smaller parts with a maximum of 25000 items each
def split_jsonl_file(data, output_prefix, max_items=25000):
    split_files = []
    current_split = 1
    current_count = 0
    split_filename = f"{output_prefix}_part{current_split}.jsonl"
    split_f = open(split_filename, 'w', encoding='utf-8')
    split_files.append(split_filename)

    for item in data:
        json.dump(item, split_f, ensure_ascii=False)
        split_f.write('\n')
        current_count += 1
        if current_count >= max_items:
            split_f.close()
            current_split += 1
            split_filename = f"{output_prefix}_part{current_split}.jsonl"
            split_f = open(split_filename, 'w', encoding='utf-8')
            split_files.append(split_filename)
            current_count = 0
    split_f.close()
    return split_files


def run_summarization_example(language_code, number_of_examples, template, prompt_name):
    """
    Runs a batch job for summarization tasks.

    Args:
        language_code (str): The language code for summarization.
        number_of_examples (int): The number of examples to process.

    Returns:
        None
    """

    # Generate language examples
    examples = create_language_examples(
        language_code, number_of_examples, MODEL, 2000)

    # Generate custom JSON data
    custom_data, total_token = generate_content_json(examples, MODEL, template)

    print("TOTAL TOKEN(INPUT):", total_token)
    costs_per_token_in = 0.150   # USD per million tokens
    costs_per_token_out = 0.600  # USD per million
    costs = total_token * (costs_per_token_in + costs_per_token_out) / (10**6)
    print("COST ESTIMATE: ", costs, "$")
    print("COST ESTIMATE with 50% Batch discount: ", costs / 2.0, "$")

    # Define input JSONL file path
    input_jsonl_file = Path(
        f'data/teaser/summarization_{language_code}_{number_of_examples}_{prompt_name.upper()}.jsonl')

    # Write custom_data to JSONL file
    with open(input_jsonl_file, 'w', encoding='utf-8') as f:
        for item in custom_data:
            f.write(json.dumps(item) + '\n')

    # Initialize OpenAI client
    client = OpenAI(api_key=API_KEY)
    max_items_per_file = 25000
    # batch_job_ids = []

    # Determine if splitting is necessary
    if len(custom_data) <= max_items_per_file:
        try:
            batch_input_file = client.files.create(
                file=open(input_jsonl_file, "rb"),
                purpose="batch"
            )
            batch_input_file_id = batch_input_file.id
            batch_job = client.batches.create(
                input_file_id=batch_input_file_id,
                endpoint="/v1/chat/completions",
                completion_window="24h",
                metadata={
                    "description": "Summarization batch job"
                }
            )
            print(f"Created batch job: {batch_job}")
        except Exception as e:
            print(f"Failed to create batch job: {e}")
    else:
        # Split JSONL file into smaller files
        split_files = split_jsonl_file(
            input_jsonl_file,
            f'data/teaser/summarization_{language_code}_{number_of_examples}_{prompt_name.upper()}_split_',
            max_items=max_items_per_file
        )

        for split_file in split_files:
            try:
                batch_input_file = client.files.create(
                    file=open(split_file, "rb"),
                    purpose="batch"
                )
                batch_input_file_id = batch_input_file.id

                batch_job = client.batches.create(
                    input_file_id=batch_input_file_id,
                    endpoint="/v1/chat/completions",
                    completion_window="24h",
                    metadata={
                        "description": f"Summarization batch job for {split_file.name}"
                    }
                )
                print(f"Created batch job for {split_file.name}: {batch_job}")
            except Exception as e:
                print(f"Failed to create batch job for {split_file.name}: {e}")


def run_summarization_batch(filename: str, template, prompt_name: str):
    with open(filename, 'r', encoding='utf-8') as json_file:
        json_data = json.load(json_file)
    # Generate custom JSON data
    custom_data, total_token = generate_content_json(
        json_data, MODEL, template, prompt_name)

    print("TOTAL TOKEN(INPUT):", total_token)
    costs_per_token_in = 0.150   # USD per million tokens
    costs_per_token_out = 0.600  # USD per million
    costs = total_token * (costs_per_token_in + costs_per_token_out) / (10**6)
    print("COST ESTIMATE: ", costs, "$")
    print("COST ESTIMATE with 50% Batch discount: ", costs / 2.0, "$")

    # Get everything but the .json extension
    base_path = os.path.splitext(filename)[0]

    # Define input JSONL file path
    input_jsonl_file = Path(f'{base_path}_{prompt_name.upper()}.jsonl')

    # Write custom_data to JSONL file
    with open(input_jsonl_file, 'w', encoding='utf-8') as f:
        for item in custom_data:
            f.write(json.dumps(item) + '\n')

    # Initialize OpenAI client
    client = OpenAI(api_key=API_KEY)
    max_items_per_file = 25000
    # batch_job_ids = []

    # Determine if splitting is necessary
    if len(custom_data) <= max_items_per_file:
        try:
            batch_input_file = client.files.create(
                file=open(input_jsonl_file, "rb"),
                purpose="batch"
            )
            batch_input_file_id = batch_input_file.id
            batch_job = client.batches.create(
                input_file_id=batch_input_file_id,
                endpoint="/v1/chat/completions",
                completion_window="24h",
                metadata={
                    "description": "Summarization batch job"
                }
            )
            print(batch_job)
        except Exception as e:
            print(f"Failed to create batch job: {e}")
    else:
        # Split JSONL file into smaller files
        split_files = split_jsonl_file(
            input_jsonl_file,
            f'data/teaser/summarization_{language_code}_{number_of_examples}_{prompt_name.upper()}_split_',
            max_items=max_items_per_file
        )

        for split_file in split_files:
            try:
                batch_input_file = client.files.create(
                    file=open(split_file, "rb"),
                    purpose="batch"
                )
                batch_input_file_id = batch_input_file.id

                batch_job = client.batches.create(
                    input_file_id=batch_input_file_id,
                    endpoint="/v1/chat/completions",
                    completion_window="24h",
                    metadata={
                        "description": f"Summarization batch job for {split_file.name}"
                    }
                )
                print(
                    f"Created batch job for {split_file.name}: {batch_job.id}")

            except Exception as e:
                print(f"Failed to create batch job for {split_file.name}: {e}")


if __name__ == "__main__":
    run_summarization_batch(
        'data/teaser/250211_latest_content_item_excluded_cba_unique.json', SHORT_INTRO_PROMPT, "SHORT_INTRO")
    run_summarization_batch(
        'data/teaser/250211_latest_content_item_excluded_cba_unique.json', LONG_INTRO_PROMPT, "LONG_INTRO")


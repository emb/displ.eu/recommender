import psycopg
from psycopg.rows import dict_row
import logging
import os
import json
import pandas as pd


INSPECT_CONTENT_ITEM = """
    SELECT column_name
    FROM information_schema.columns 
    WHERE table_name = 'ContentItem'
    AND table_schema = 'public';
"""

INSPECT_CONTENT_ITEM_TRANSLATION = """
    SELECT column_name
    FROM information_schema.columns 
    WHERE table_name = 'ContentItemTranslation'
    AND table_schema = 'public';
"""

UPDATE_CONTENT_ITEM_SHORT = """
    UPDATE "ContentItem"
    SET "autoTeaserShort" = %s
    WHERE uid = %s;
"""

UPDATE_CONTENT_ITEM_LONG = """
    UPDATE "ContentItem"
    SET "autoTeaserLong" = %s
    WHERE uid = %s;
"""

UPDATE_CONTENT_ITEM_TRANSLATION_SHORT = """
    UPDATE "ContentItemTranslation"
    SET "autoTeaserShort" = %s
    WHERE "contentItemUid" = %s;
"""

UPDATE_CONTENT_ITEM_TRANSLATION_LONG = """
    UPDATE "ContentItemTranslation"
    SET "autoTeaserLong" = %s
    WHERE "contentItemUid" = %s;
"""

def get_database_connection():
    try:
        # Connect to the database
        conn = psycopg.connect(
            dbname="repco-live-10.2",
            host=os.getenv("DB_HOST", "localhost"),
            port=os.getenv("DB_PORT", "5432"),
            user=os.getenv("DB_USER", "repco"),
            password=os.getenv("DB_PASSWORD"),
        )
        print(f"Connected to the database: {conn}")
        return conn
    except Exception as e:
        logging.error(f"Error connecting to the database: {e}")
        return None

def execute_query(query: str):
    try:
        # Create a cursor
        conn = get_database_connection()
        cursor = conn.cursor(row_factory=dict_row)
        cursor.execute(query)
        result = cursor.fetchall()
        print(result)
        print(f"Sucessfully executed query")
        # Close the connection
        cursor.close()
        conn.close()
        print(f"Closed the connection")
        return result
    except Exception as e:
        logging.error(f"Error executing query: {e}")

def inspect_db():
    print(f"Inspect ContentItem")
    result = execute_query(INSPECT_CONTENT_ITEM)
    print(f"Inspect ContentItemTranslation")
    result = execute_query(INSPECT_CONTENT_ITEM_TRANSLATION)


def split_custom_id_column(df: pd.DataFrame, col_name: str = 'custom_id') -> pd.DataFrame:
    """
    Given a DataFrame containing a string column in the form 'uid_revision_fieldname_src_lang[_target_lang]',
    split the column into separate new columns based on the number of fields.

    :param df: The input pandas DataFrame.
    :param col_name: The name of the column to split. Defaults to 'custom_id'.
    :return: The modified DataFrame with new columns.
    """
    split_values = df[col_name].str.split('_', expand=True)
    
    if split_values.shape[1] == 4:  # If there are 4 parts
        df[['uid', 'revision', 'fieldname', 'src_lang']] = split_values
    elif split_values.shape[1] == 5:  # If there are 5 parts
        df[['uid', 'revision', 'fieldname', 'src_lang', 'target_lang']] = split_values
    return df


def parse_jsonl_to_dataframe(file_path: str) -> pd.DataFrame:
    data = []
    with open(file_path, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.strip()
            if not line:
                continue

            record = json.loads(line)
            custom_id = record.get("custom_id")

            choices = record.get("response", {}) \
                            .get("body", {}) \
                            .get("choices", [])

            for choice in choices:
                if choice.get("finish_reason") == "stop":
                    content = choice.get("message", {}).get("content")
                    data.append({
                        "custom_id": custom_id,
                        "content": content
                    })
    # Build the dataframe
    df = pd.DataFrame(data, columns=["custom_id", "content"])
    df = split_custom_id_column(df)
    return df
    

def parse_json_to_dataframe(file_path: str) -> pd.DataFrame:
    data = []
    # Load JSON file
    with open(file_path, "r", encoding="utf-8") as file:
        data = json.load(file)
    # Build the dataframe
    df = pd.DataFrame(data, columns=["custom_id", "content"])
    df = split_custom_id_column(df)
    return df


def load_teaser_into_contentItem():
    try:
        conn = get_database_connection()
        cursor = conn.cursor(row_factory=dict_row)

        # Update to autoTeaserShort column in ContentItem
        json_unchanged_file_path_short = "data/teaser/output/unchanged/no_summarization_SHORT_INTRO.json"
        # Convert JSON to DataFrame
        df_unchanged_short = parse_json_to_dataframe(json_unchanged_file_path_short)
        for index,row in df_unchanged_short.iterrows():
            cursor.execute(UPDATE_CONTENT_ITEM_SHORT, (json.dumps(row['content']), row['uid']))
        print(f"Sucessfully updating unchanged content to short summary")

        jsonl_teaser_file_path_short = "data/teaser/output/origin_teasers/batch_67b8393a83b8819091cc6b471b44ef35_output.jsonl"
        df_teasers_short = parse_jsonl_to_dataframe(jsonl_teaser_file_path_short)
        for index,row in df_teasers_short.iterrows():
            cursor.execute(UPDATE_CONTENT_ITEM_SHORT, (row['content'], row['uid']))
        print(f"Sucessfully updating teasers to short summary")

        # Update to autoTeaserLong column in ContentItem
        # XXX
        
    except Exception as e:
        print(f"OOOOPS!!!!. Error: {str(e)}")
    finally:
        conn.commit()
        cursor.close()
        conn.close()
        print(f"Closed the connection")


def load_teaser_into_contentItemTranslation():
    try:
        conn = get_database_connection()
        cursor = conn.cursor(row_factory=dict_row)

        # Update to autoTeaserShort column in ContentItemTranslation
        json_unchanged_file_path_short = "data/teaser/output/translated_unchanged/batch_67b858cdabe481909c1953ed82d608f8_output.jsonl"
        # Convert JSON to DataFrame
        df_unchanged_short = parse_jsonl_to_dataframe(json_unchanged_file_path_short)
        for index,row in df_unchanged_short.iterrows():
            cursor.execute(UPDATE_CONTENT_ITEM_TRANSLATION_SHORT, (json.dumps(row['content']), row['uid']))
        print(f"Sucessfully updating unchanged content to autoTeaserShort")

        jsonl_teaser_file_path_short = "data/teaser/output/translated_teasers/batch_67b84390075881909a58efe60d5300e9_output.jsonl"
        df_teasers_short = parse_jsonl_to_dataframe(jsonl_teaser_file_path_short)
        for index,row in df_teasers_short.iterrows():
            cursor.execute(UPDATE_CONTENT_ITEM_TRANSLATION_SHORT, (row['content'], row['uid']))
        print(f"Sucessfully updating teasers to autoTeaserShort")

        # Update to autoTeaserLong column in ContentItemTranslation
        json_unchanged_file_path_long = "data/teaser/output/translated_unchanged/batch_67b858cfacf88190a04f6d1e2ea187b7_output.jsonl"
        # Convert JSON to DataFrame
        df_unchanged_long = parse_jsonl_to_dataframe(json_unchanged_file_path_long)
        for index,row in df_unchanged_long.iterrows():
            cursor.execute(UPDATE_CONTENT_ITEM_TRANSLATION_LONG, (json.dumps(row['content']), row['uid']))
        print(f"Sucessfully updating unchanged content to autoTeaserLong")

        jsonl_teaser_file_path_long = "data/teaser/output/translated_teasers/batch_67b843925888819082ab3d9451d40a71_output.jsonl"
        df_teasers_long = parse_jsonl_to_dataframe(jsonl_teaser_file_path_long)
        for index,row in df_teasers_long.iterrows():
            cursor.execute(UPDATE_CONTENT_ITEM_TRANSLATION_LONG, (row['content'], row['uid']))
        print(f"Sucessfully updating teasers to autoTeaserLong")
    
    except Exception as e:
        print(f"OOOOPS!!!!. Error: {str(e)}")
    finally:
        conn.commit()
        cursor.close()
        conn.close()
        print(f"Closed the connection")


if __name__ == "__main__":
    inspect_db()
    # load_teaser_into_contentItem()
    # load_teaser_into_contentItemTranslation()
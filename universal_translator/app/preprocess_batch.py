import logging
import json
import tiktoken
from app.constant import INPUT_JSON_FILE_PATH, INPUT_JSONL_FILE_PATH, LANGUAGE_CODES, COSTS_PER_TOKEN_IN, COSTS_PER_TOKEN_OUT

def num_tokens_from_string(string: str, encoding_name: str) -> int:
    try:
        encoding = tiktoken.encoding_for_model(encoding_name)
        num_tokens = len(encoding.encode(string))
        return num_tokens
    except Exception as e:
        logging.error(f"Error in num_tokens_from_string: {e}")
        return 0

def create_batch_item(uid, revisionid, field, content, source_language_code, target_language_code, model, max_tokens, template):
    target_language_name = next((name for name, code in LANGUAGE_CODES.items() if code == target_language_code), None)
    try:
        return {
            "custom_id": f"{uid}_{revisionid}_{field}_{source_language_code}_{target_language_code}",
            "method": "POST",
            "url": "/v1/chat/completions",
            "body": {
                "model": model,
                "messages": [
                    {"role": "system", "content": template.format(TARGET_LANGUAGE=target_language_name)},
                    {"role": "user", "content": content}
                ],
                "max_tokens": max_tokens,
                "temperature": 0,
                "top_p": 1,
                "frequency_penalty": 0,
                "presence_penalty": 0,
                "seed": 42
            }
        }
    except Exception as e:
        logging.error(f"Error in create_batch_item: {e}")
        return {}

# Function to generate the custom JSON format
def generate_batch_custom_json(data, model, max_tokens, template, target_language_code):
		total_token = 0
		custom_data = []
		try:
			for entry in data:
				uid = entry.get("uid")
				revisionid = entry.get("revisionId")
				original_langs = entry.get("originalLanguages", {})
				# If "language_codes" are provided and nonempty, use them.
				if original_langs and "language_codes" in original_langs and original_langs["language_codes"]:
					language_codes = original_langs["language_codes"]
				else:
					# Otherwise, derive language_codes from non-empty values in title, summary, and content.
					language_codes_set = set()
					for field in ["title", "summary", "content"]:
						field_data = entry.get(field, {})
						for lang_code, lang_info in field_data.items():
							if lang_info.get("value", "").strip():
								language_codes_set.add(lang_code)
					language_codes = list(language_codes_set)
					if not language_codes:
						language_codes = ["en"]
				unique_language_codes = set(language_codes)		    

				# Process each language in language_codes
				for source_language_code in unique_language_codes:
						fields = ["title", "summary", "content"]
		
						for field in fields:
								field_data = entry.get(field, {})
								content_dict = field_data.get(source_language_code, {})
								field_content = content_dict.get("value", "").strip()

								if field_content:
										field_token = num_tokens_from_string(field_content, model)
										total_token += min(field_token, max_tokens)
										custom_item = create_batch_item(uid, revisionid, field, field_content, source_language_code, target_language_code, model, max_tokens, template)
										custom_data.append(custom_item)
		except Exception as e:
			logging.error(f"Error while generating batch custom JSON: {e}")
			return [], 0
		return custom_data, total_token

def preprocess_batch_data(target_language: str, model: str, max_tokens: int, template: str):
    # Generate JSON data
    total_tokens = 0
    output_json_datas = []
    try:
        with open(INPUT_JSON_FILE_PATH, 'r', encoding='utf-8') as json_file:
            json_data = json.load(json_file)
            
        logging.info(f"Number of input items: {len(json_data)}")
        
        if target_language.lower() == 'all':
            logging.info(f"Number of translating languages: {len(LANGUAGE_CODES)}")
            
            for target_language_code in LANGUAGE_CODES.values():
                output_json_data, total_token = generate_batch_custom_json(
                    json_data, model, max_tokens, template, target_language_code)
                output_json_datas.extend(output_json_data)
                total_tokens += total_token
                
            with open(INPUT_JSONL_FILE_PATH, 'w', encoding='utf-8') as json_file:
                for item in output_json_datas:
                    json_file.write(json.dumps(item, ensure_ascii=False) + '\n')     
                           
            logging.info(f"Exported to {INPUT_JSONL_FILE_PATH}")
            return output_json_datas
            
        elif target_language.lower() in LANGUAGE_CODES.values():
            output_json_datas, total_tokens = generate_batch_custom_json(
                json_data, model, max_tokens, template, target_language)
            
            with open(INPUT_JSONL_FILE_PATH, 'w', encoding='utf-8') as json_file:
                for item in output_json_datas:
                    json_file.write(json.dumps(item, ensure_ascii=False) + '\n')
                    
            logging.info(f"Exported to {INPUT_JSONL_FILE_PATH}")
            return output_json_datas
        else:
            logging.error(f"Language code {target_language} not supported, please choose another language code or 'all'")
            return
        
    except Exception as e:
        logging.error(f"Error while preprocess batch data: {e}")
        return
    
    finally:
        logging.info("="*80)
        logging.info(f"TOTAL TOKEN(INPUT): {total_tokens}")
        costs = total_tokens * (COSTS_PER_TOKEN_IN + COSTS_PER_TOKEN_OUT) / (10**6)
        logging.info(f"COST ESTIMATE with 50 percent Batch discount: {costs / 2.0}$")

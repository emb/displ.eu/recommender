import logging
import json
import os
from app.constant import TRANSLATION_BATCH_JOBS_TRACKING_FILE_PATH, OPENAI_OUTPUT_DIR


def process_batch_jobs(client, tracking_file_path=TRANSLATION_BATCH_JOBS_TRACKING_FILE_PATH):
    """
    Processes all batch jobs by retrieving output files, extracting and exporting data,
    creating evaluation Excel files, and handling errors by logging them.

    Parameters:
    - tracking_file (str): Path to the tracking JSON file.

    Returns:
    - None
    """
    # Load tracking data
    try:
        with open(tracking_file_path, 'r', encoding='utf-8') as f:
            tracking_data = json.load(f)
    except FileNotFoundError:
        logging.error(f"Tracking file {tracking_file_path} not found.")
        return
    except json.JSONDecodeError:
        logging.error(f"Error decoding JSON from {tracking_file_path}.")
        return

    # Iterate over each input file and its associated batch_job_id
    for input_file, details in tracking_data.items():
        status = details.get("status", "pending")
        if status == "processed":
            logging.info(f"Skipping already processed file: {input_file}")
            continue  # Skip already processed files

        print('START PROCESSING', input_file)
        batch_job_id = details.get('batch_job_id', [])
        if not batch_job_id:
            logging.warning(f"No batch_job_id found for {input_file}.")
            continue

        error_records = []  # To store error information

        # Retrieve batch job details
        batch_job = client.batches.retrieve(batch_job_id)
        output_file_id = batch_job.output_file_id
        error_file_id = batch_job.error_file_id

        if output_file_id:
            # Fetch the output file
            output_file_content = client.files.content(output_file_id)
            if not output_file_content:
                logging.error(f"Failed to fetch output content for batch_job_id {batch_job_id}.")
                continue

            output_file = os.path.join(OPENAI_OUTPUT_DIR, f"{batch_job_id}_output.jsonl")

            output_lines = output_file_content.text.splitlines()
            with open(output_file, 'w', encoding='utf-8') as outfile:
                for line in output_lines:
                    outfile.write(line + '\n')           

            logging.info(f"Processed batch_job_id {batch_job_id} successfully.")
            # Mark the input_file as processed
            tracking_data[input_file]["status"] = "processed"

        # Handle error_file_id if present
        if error_file_id:
            error_content = client.files.content(error_file_id)
            if error_content:
                error_lines = error_content.text.splitlines()
                for error_line in error_lines:
                    error_dict = json.loads(error_line)
                    if error_dict.get("error", {}):
                        error_records.append({
                            "custom_id": error_dict.get("custom_id", {}),
                            "error_message": error_dict.get("error", {}).get("message", "")
                        })
                    else:
                        error_records.append({
                            "custom_id": error_dict.get("custom_id", {}),
                            "error_message": ""
                        })

        # Save error records if any
        if error_records:
            error_file = os.path.join(OPENAI_OUTPUT_DIR, f"{batch_job_id}_error.json")
            with open(error_file, 'w', encoding='utf-8') as error_file:
                json.dump(error_records, error_file, ensure_ascii=False, indent=4)
            logging.info(f"Logged errors to {error_file}.")

        # Update the tracking file
        try:
            with open(tracking_file_path, 'w', encoding='utf-8') as f:
                json.dump(tracking_data, f, ensure_ascii=False, indent=4)
            logging.info(f"Updated tracking file {tracking_file_path}.")
        except IOError as e:
            logging.error(f"Failed to update tracking file {tracking_file_path}: {e}")

        logging.info('---------------------------------------------')


def retry_pending_batch_jobs(client, tracking_file_path=TRANSLATION_BATCH_JOBS_TRACKING_FILE_PATH):
    """
    Reads the tracking file and retries any batch jobs with status 'pending'.
    Updates the tracking file with new batch_job_id after resubmission.

Args:
        tracking_file_path (str): Path to the tracking JSON file.
        api_key (str): OpenAI API key.
    """
    with open(tracking_file_path, 'r', encoding='utf-8') as file:
        tracking_data = json.load(file)

    updated = False

    for key, value in tracking_data.items():
        if value.get("status") == "pending":
            input_file = value.get("input_file")
            if not input_file or not os.path.exists(input_file):
                logging.error(f"Input file {input_file} does not exist. Skipping.")
                continue
            batch_job_id = value.get("batch_job_id")
            saved_batch = client.batches.retrieve(batch_job_id)
            if saved_batch.status == 'failed':
                try:
                    # Resubmit the batch job
                    batch_input_file = client.files.create(
                        file=open(input_file, "rb"),
                        purpose="batch"
                    )
                    batch_input_file_id = batch_input_file.id

                    batch_job = client.batches.create(
                        input_file_id=batch_input_file_id,
                        endpoint="/v1/chat/completions",
                        completion_window="24h",
                        metadata={
                            "description": f"Retry batch job for {input_file}"
                        }
                    )
                    logging.info(f"Resubmitted batch job for {input_file}: {batch_job.id}")

                    # Update the tracking data
                    tracking_data[key]["batch_job_id"] = batch_job.id
                    tracking_data[key]["status"] = "processed"
                    updated = True

                except Exception as e:
                    logging.error(f"Failed to resubmit batch job for {input_file}: {e}")

    if updated:
        # Overwrite the original tracking file with updated data
        with open(tracking_file_path, 'w', encoding='utf-8') as file:
            json.dump(tracking_data, file, ensure_ascii=False, indent=4)
        logging.info(f"Tracking file {tracking_file_path} has been updated with new batch_job_id.")
    else:
        logging.info("No pending batch jobs found to retry.")

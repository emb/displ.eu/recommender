import os
from datetime import datetime
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

# Get today's date in YYYY-MM-DD format
today = datetime.today().strftime('%Y-%m-%d')

API_KEY = os.getenv('OPENAI_API_KEY')

MODEL = "gpt-4o-mini"
ENGINE = 'gpt-4o-mini-2024-07-18'
MAX_TOKENS = 16000
COSTS_PER_TOKEN_IN = 0.150   # USD per million tokens
COSTS_PER_TOKEN_OUT = 0.600  # USD per million
MAX_ITEMS_PER_FILE = 25000

DATA_DIR = "data"
OPENAI_INPUT_DIR = "data/input"
OPENAI_OUTPUT_DIR = "data/output"

INPUT_JSON_FILE_NAME = f"{today}_content_items.json"
INPUT_JSONL_FILE_NAME = f"{today}_content_items.jsonl"
INPUT_SPLIT_JSONL_FILE_PREFIX = f"{today}_content_items_split"
TRANSLATION_BATCH_JOBS_TRACKING_FILE_NAME = "translation_batch_jobs_tracking.json"
MISSING_UIDS_FILE_NAME = "missing_uids.json"

INPUT_JSON_FILE_PATH = os.path.join(DATA_DIR, INPUT_JSON_FILE_NAME)
INPUT_JSONL_FILE_PATH = os.path.join(OPENAI_INPUT_DIR, INPUT_JSONL_FILE_NAME)
INPUT_SPLIT_JSONL_FILE_PATH = os.path.join(
    OPENAI_INPUT_DIR, INPUT_SPLIT_JSONL_FILE_PREFIX)
TRANSLATION_BATCH_JOBS_TRACKING_FILE_PATH = os.path.join(
    DATA_DIR, TRANSLATION_BATCH_JOBS_TRACKING_FILE_NAME)
MISSING_UIDS_FILE_PATH = os.path.join(DATA_DIR, MISSING_UIDS_FILE_NAME)

LANGUAGE_CODES = {
    "English": "en",
    "German": "de",
    "Croatian": "hr",
    "Serbian": "sr",
    # "Bosnian": "bs", # Croatian and Bosnian are very similar, so only run Croatian
    "Czech": "cs",
    "Slovakian": "sk",
    "French": "fr",
    "Hungarian": "hu",
    "Italian": "it",
    "Polish": "pl",
    "Spanish": "es",
    "Dutch": "nl",
    "Portuguese": "pt",
    "Ukrainian": "uk",
    "Russian": "ru",
    "Turkish": "tr",
    "Romanian": "ro",
    "Bulgarian": "bg",
    "Finnish": "fi",
    "Greek": "el",
    "Swedish": "sv"
}

SQL_SELECT_LATEST_CONTENT_ITEMS = """
SELECT DISTINCT ON (CI."uid")
    CI."uid",
    CI."revisionId",
    CI."title",
    CI."summary",
    CI."content",
    CI."originalLanguages"
FROM public."Revision" AS R
JOIN public."ContentItem" AS CI ON R."uid" = CI."uid"
LEFT JOIN public."ContentItemTranslation" AS CIT ON CIT."contentItemUid" = CI."uid"
WHERE CIT."contentItemUid" IS NULL
AND R."dateCreated" > '2025-01-21 16:45:29'
AND R."repoDid" NOT IN (
    'did:key:z6MkesvwNeSzVtFxhxPo4sy6FZ89jyCWVAU8tnS9j2MnTedH',
    'did:key:z6MkeUvEVvz9PkG4QSx5p4ABa9ffWRGXfKDcyz5SsvidLTny9j2MnTedH',
    'did:key:z6MkfFYPhnwcEJpFVantvcnTTNoixVCocftFrEkzWiZk25SF',
    'did:key:z6MkfqoYG7hANwWdRbaSdxJLScnQLRhu3NViZp2eq437UWoL',
    'did:key:z6Mkg7vmq4sMzw4DHA24L53ENjZ2972pCKXTJevAFVNX7Aon',
    'did:key:z6MkgY9qfehdSR9x8sb8tHt5ufEapTZ54LDL4M1eTqnKKxi1on',
    'did:key:z6Mkhu95ctXhL2VhRgdNap74eGDQF8tCwQDCFtmV2HY2Lj2w',
    'did:key:z6MkjLR38y93qSfhd1aocd8LRSmkkVy7SiYcbXebLqUzPbJQ',
    'did:key:z6Mkjqu6Zd86t6BK4yrFDts1sbTk8JhT6YjGLfKjPP55Aq4f',
    'did:key:z6MkkfEXK3SXTRqz99KzaFgHz7sC3vzcpc56ZsC6ScqQYw6Q',
    'did:key:z6MknesfWdVWfDJ45SZxeh3nzp5w1bbiMmhrX56M8jS3Xp1W',
    'did:key:z6MknYnFUAxuFMF3JyXyhG5bhukqGk1Be8xJqwiz1ifLPrMy',
    'did:key:z6Mko4CAnBMHakGLoYdaw9mTmhr5ZncK49zUZZ6Dx3CprHPD',
    'did:key:z6MkoD6TDiz46mhpiFk1hE7jCFKgdhjSuyT3vAR9Vkqpkb3q',
    'did:key:z6MkpMttBAPmxjtBU8LUX3Rrche3rjT1o1xumgAQaacSBiPb',
    'did:key:z6MkpxZrFEVB67N9J43JavSNjM4YzsBoQP4xcBbCu9hBwJFq',
    'did:key:z6Mkr3qprqrA7LNZsbBJUJYP39BZZYZ4wNQVTxzdDLQQk8nD',
    'did:key:z6Mks9QVryL1S2XrdXq5GsMuJbE8p9kteAPTLjCu1P9h3kph',
    'did:key:z6MksEa7o5JbtCqBAguTbKug9LJAF3z7UrEHobbS1T4wuVnQ',
    'did:key:z6Mksh4mPdmbGWAaNN9dZdQCPJzFNe6Sskf3ymnhGZtyVLpM',
    'did:key:z6MkuyLvkJZfTe71sBy4fNJanqz9dpxqFeWXLSRLuQVw6xhk',
    'did:key:z6MkwHNjWpR3D81i69joSj2rgEL9Yxyv2MjWP5SKGtrKvLyS',
    'did:key:z6MksJJniFL8NSnkeaJ5kijw1BWeHgF6DFkhZZk4Xg3d5WPA'
)
ORDER BY CI."uid", R."dateModified" DESC;
"""

SQL_COUNT_TRANSLATION_LANGUAGES = """
    SELECT count(*) AS cnt, "targetLanguage" AS lang
    FROM "ContentItemTranslation"
    GROUP BY lang
    ORDER BY cnt DESC;
"""

SQL_INSERT_CONTENT_ITEM_TRANSLATION = """INSERT INTO "ContentItemTranslation" (
        "targetLanguage", 
        "{X}", 
        "contentItemUid", 
        "revisionId", 
        "createdAt", 
        "updatedAt", 
        engine, 
        metadata) 
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
    ON CONFLICT ("contentItemUid", "revisionId", "targetLanguage")
    DO UPDATE
        SET "targetLanguage" = %s,
        "{X}" = %s,
        "updatedAt" = %s,
        engine = %s,
        metadata = %s 
"""

SQL_CHECK_EXISTENCE_CONTENT_ITEM_TRANSLATION = """
    SELECT COUNT(*) AS cnt FROM "ContentItemTranslation" 
    WHERE "contentItemUid" = %s AND "revisionId" = %s AND "targetLanguage" = %s
"""

PROMPT_KEEP_HTML_TRANSLATE = """
Your task is to translate the provided text into **{TARGET_LANGUAGE}**, 
while preserving the HTML formatting, CSS styles, and any JavaScript code embedded within the text. 
Additionally, maintain all whitespace characters such as line breaks (`\\n`). 
If the input text is already in **{TARGET_LANGUAGE}**, repeat the text exactly as it is.

**Instructions:**

- **Translate the text into {TARGET_LANGUAGE}.** If the text is already in {TARGET_LANGUAGE}, repeat it word for word.
- **Preserve all HTML tags and formatting** in their original positions within the text.
- **Preserve all CSS styles** within `<style>` tags without any modifications.
- **Preserve all JavaScript code** within `<script>` tags without any modifications.
- **Preserve all whitespace characters**, including line breaks (`\\n`), exactly as they appear in the input.

**Constraints:**

- Do **not** add any text.
- Do **not** change any content or alter the meaning.
- Do **not** provide any explanations, comments, or additional information.
- Do **not** shorten or summarize the text.

**Output:**

- Only output the translated text, preserving the HTML formatting, CSS styles, JavaScript code, and whitespace as specified.
"""

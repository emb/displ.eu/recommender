import os
import logging
import json
from dotenv import load_dotenv
import psycopg
from psycopg.rows import dict_row
from typing import List, Dict
from app.constant import INPUT_JSON_FILE_PATH, SQL_SELECT_LATEST_CONTENT_ITEMS

load_dotenv()


def get_database_connection():
    # Connect to the database
    conn = psycopg.connect(
        dbname="repco-live-10.2",
        host=os.getenv("DB_HOST", "localhost"),
        port=os.getenv("DB_PORT", "5432"),
        user=os.getenv("DB_USER", "repco"),
        password=os.getenv("DB_PASSWORD"),
    )
    print(conn)
    return conn


def execute_query(query: str) -> List[Dict]:
    try:
        # Create a cursor
        conn = get_database_connection()
        cursor = conn.cursor(row_factory=dict_row)
        cursor.execute(query)
        result = cursor.fetchall()
        logging.info("Sucessfully executed query")
    except Exception as e:
        logging.error(f"Error executing query: {e}")
    finally:
        # Close the connection
        cursor.close()
        conn.close()
    return result


def export_latest_content_items():
    try:
        result = execute_query(SQL_SELECT_LATEST_CONTENT_ITEMS)
        # Exporting to JSON file
        with open(INPUT_JSON_FILE_PATH, 'w', encoding='utf-8') as json_file:
            json.dump(result, json_file, ensure_ascii=False, indent=4, default=str)

        logging.info(f"Saved {len(result)} content items to {INPUT_JSON_FILE_PATH}")

    except Exception as e:
        logging.error(f"Error exporting latest content items: {e}")
    return result

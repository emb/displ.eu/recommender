import json
import logging
from app.constant import MAX_ITEMS_PER_FILE, INPUT_JSONL_FILE_PATH, INPUT_SPLIT_JSONL_FILE_PREFIX, TRANSLATION_BATCH_JOBS_TRACKING_FILE_PATH


# Function to split a JSONL file into smaller parts with a maximum of 25000 items each
def split_jsonl_file(data, output_prefix, max_items_per_file=MAX_ITEMS_PER_FILE):
    split_files = []
    current_split = 1
    current_count = 0
    split_filename = f"{output_prefix}_part{current_split}.jsonl"
    split_f = open(split_filename, 'w', encoding='utf-8')
    split_files.append(split_filename)

    for item in data:
        json.dump(item, split_f, ensure_ascii=False)
        split_f.write('\n')
        current_count += 1
        if current_count >= max_items_per_file:
            split_f.close()
            current_split += 1
            split_filename = f"{output_prefix}_part{current_split}.jsonl"
            split_f = open(split_filename, 'w', encoding='utf-8')
            split_files.append(split_filename)
            current_count = 0
    split_f.close()
    return split_files


def log_batch_job(input_file, batch_job_id, tracking_file_path=TRANSLATION_BATCH_JOBS_TRACKING_FILE_PATH):
    """
    Logs the input JSONL file and corresponding batch job IDs to a tracking file.

    Args:
        input_file (str): Path to the input JSONL file.
        batch_job_id (str): batch job ID.
        tracking_file (str): Path to the tracking file.
    """
    tracking_data = {}

    # Load existing tracking data if the file exists
    if tracking_file_path.exists():
        with open(tracking_file_path, 'r', encoding='utf-8') as f:
            try:
                tracking_data = json.load(f)
            except json.JSONDecodeError:
                tracking_data = {}

    # Update tracking data
    tracking_data[input_file] = {
        "input_file": input_file,
        "batch_job_id": batch_job_id,
        "status": "pending"
    }

    logging.info(f"Logged batch job for {input_file}: {batch_job_id}")

    # Write back to the tracking file
    with open(tracking_file_path, 'w', encoding='utf-8') as f:
        json.dump(tracking_data, f, ensure_ascii=False, indent=4)


def send_batch_request(client, max_items_per_file=MAX_ITEMS_PER_FILE):

    with open(INPUT_JSONL_FILE_PATH, 'r', encoding='utf-8') as json_file:
        output_json_datas = json.load(json_file)

    if len(output_json_datas) <= max_items_per_file:
        try:
            batch_input_file = client.files.create(
                file=open(INPUT_JSONL_FILE_PATH, "rb"),
                purpose="batch"
            )
            batch_input_file_id = batch_input_file.id
            batch_job = client.batches.create(
                input_file_id=batch_input_file_id,
                endpoint="/v1/chat/completions",
                completion_window="24h",
                metadata={
                    "description": "nightly translation job"
                }
            )

            logging.info(
                f"Created batch job for {INPUT_JSONL_FILE_PATH}: {batch_job.id}")
            log_batch_job(INPUT_JSONL_FILE_PATH, batch_job.id)

        except Exception as e:
            logging.error(
                f"Error creating batch job for {INPUT_JSONL_FILE_PATH}: {e}")
    else:

        split_files = split_jsonl_file(
            output_json_datas, INPUT_SPLIT_JSONL_FILE_PREFIX, max_items_per_file)

        for split_file in split_files:
            try:
                batch_input_file = client.files.create(
                    file=open(split_file, "rb"),
                    purpose="batch"
                )
                batch_input_file_id = batch_input_file.id
                batch_job = client.batches.create(
                    input_file_id=batch_input_file_id,
                    endpoint="/v1/chat/completions",
                    completion_window="24h",
                    metadata={
                        "description": f"Batch job for {split_file}"
                    }
                )

                logging.info(
                    f"Created batch job for {split_file}: {batch_job.id}")
                log_batch_job(split_file, batch_job.id)

            except Exception as e:
                logging.error(
                    f"Error creating batch job for {split_file}: {e}")

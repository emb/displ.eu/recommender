import os
import json
from datetime import datetime
import logging
import pandas as pd
from app.fetch_from_database import get_database_connection
from app.constant import OPENAI_OUTPUT_DIR, SQL_INSERT_CONTENT_ITEM_TRANSLATION, SQL_CHECK_EXISTENCE_CONTENT_ITEM_TRANSLATION, ENGINE


def split_custom_id_column(df: pd.DataFrame, col_name: str = 'custom_id') -> pd.DataFrame:
    """
    Given a DataFrame containing a string column in the form 'uid_fieldname_language',
    split the column into three new columns: 'uid', 'fieldname', 'language'.

    :param df: The input pandas DataFrame.
    :param col_name: The name of the column to split. Defaults to 'custom_id'.
    :return: The modified DataFrame with three new columns.
    """
    df[['uid', 'revision', 'fieldname', 'src_lang', 'target_lang']
       ] = df[col_name].str.split('_', expand=True)
    return df


def parse_jsonl_to_dataframe(file_path: str) -> pd.DataFrame:
    """Parse and convert to a pandas dataframe what we get from openAi's batch results.
    We get:

    ... example ...
    """
    data = []
    with open(file_path, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.strip()
            if not line:
                continue

            record = json.loads(line)
            custom_id = record.get("custom_id")

            choices = record.get("response", {}) \
                            .get("body", {}) \
                            .get("choices", [])

            for choice in choices:
                if choice.get("finish_reason") == "stop":
                    content = choice.get("message", {}).get("content")
                    data.append({
                        "custom_id": custom_id,
                        "content": content
                    })
                # elif: should we log somewhere that it did not work???? XXX

    # Build the dataframe
    df = pd.DataFrame(data, columns=["custom_id", "content"])
    df = split_custom_id_column(df)
    return df


def load_into_contentItemTranslation(df: pd.DataFrame):
    """INSERT or UPDATE (UPSERT) each translated row into the right row of 
    "ContentItemTranslation in repco-live-16.2"""

    if 'fieldname' in df and df['fieldname'] == 'title':
        SQL = SQL_INSERT_CONTENT_ITEM_TRANSLATION.format(X='title')
        field = df['content']
    elif 'fieldname' in df and df['fieldname'] == 'summary':
        SQL = SQL_INSERT_CONTENT_ITEM_TRANSLATION.format(X='summary')
        field = df['content']
    elif 'fieldname' in df and df['fieldname'] == 'content':
        SQL = SQL_INSERT_CONTENT_ITEM_TRANSLATION.format(X='content')
        field = df['content']
    else:
        logging.warning(
            f"Warning: did not find 'title', 'summary' nor 'content in the df. df = {df}")
        return

    conn = get_database_connection()
    cursor = conn.cursor()

    missing_uids = set()
    now = datetime.today()

    metadata = None

    cursor.execute(SQL_CHECK_EXISTENCE_CONTENT_ITEM_TRANSLATION,
                   (df['uid'], df['revision'], df['target_lang']))
    result = cursor.fetchone()

    # Determine if an entry exists by checking the count
    if result is not None:
        try:
            # If the cursor returns a dict (e.g., RealDictCursor)
            count = result["cnt"]
        except (KeyError, TypeError):
            # Otherwise assume it's a tuple-like object
            count = result[0]
    else:
        count = 0

    exists = count > 0

    try:
        if not exists:
            cursor.execute(SQL, (df['target_lang'], df['content'], df['uid'], df['revision'],
                           now, None, ENGINE, metadata, df['target_lang'], field, now, ENGINE, metadata))
    except Exception as e:
        logging.error(f"Error inserting into ContentItemTranslation: {str(e)}")
        missing_uids.add(df['uid'])
    finally:
        conn.commit()
        cursor.close()
        conn.close()
    return missing_uids


def run_import_to_database():
    for file_name in os.listdir(OPENAI_OUTPUT_DIR):
        if file_name.endswith(".jsonl"):
            file_path = os.path.join(OPENAI_OUTPUT_DIR, file_name)
        df_results = parse_jsonl_to_dataframe(file_path)
        for index, row in df_results.iterrows():
            load_into_contentItemTranslation(row)

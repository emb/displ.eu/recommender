# Description

The project, its portal and its outposts, offer content recommendations, rating and the ability to comment for its users. 
Tools to facilitate moderaton and abuse-handling will be implemented and provided.

# Sub-projects

## Universal Translator

Directory [universal_translator](universal_translator)

This module contains code for running batch translations with openai's GPT models.
It is intended to run via cron job.
Input data comes from repco (via an API) and output translations are stored into repco.


## Recommender

## vector-search



# test scripts for vector search DBs

- Run Qdrant docker
```
sudo docker run -p 6333:6333 -p 6334:6334 -v $(pwd)/qdrant_storage:/qdrant/storage:z qdrant/qdrant
``` 

- Create collections:
```
python import_db.py
``` 

- Run FastAPI (API in api.py)
```
uvicorn api:app --reload --port 8000
``` 


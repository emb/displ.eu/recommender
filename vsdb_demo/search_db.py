from qdrant_client import QdrantClient, models
import torch
import json
from sentence_transformers import SentenceTransformer
from vsdb_demo.settings import DB_CONNECTION_STRING, DB_NAME, VECTOR_DB_NAME


# def search_db(query: str, limit: int = 5):
#     client = QdrantClient(url=DB_CONNECTION_STRING, port=None)
#     results = client.query(collection_name=DB_NAME, 
#                            query_text=query, 
#                            limit=limit)
#     return results

def search_vector_db(query: str, filter="en", limit: int = 5):
    results = []
    client = QdrantClient(url=DB_CONNECTION_STRING, port=None)
    # Initialize encoder (modify to CPU if needed)
    encoder = SentenceTransformer("all-MiniLM-L6-v2", device='cuda' if torch.cuda.is_available() else 'cpu')
    
    # Query without filter
    hits = client.query_points(
        collection_name=VECTOR_DB_NAME,
        query=encoder.encode(query).tolist(),
        query_filter=models.Filter(
            must=[models.FieldCondition(key="language", match=models.MatchValue(value=filter))]
    ),
        limit=limit,
    ).points

    for hit in hits:
        custom_id = hit.payload["custom_id"]
        custom_id_split = custom_id.split("_")
        result = {
            "id": custom_id_split[0],
            "revisionId": custom_id_split[1],
            "contentType": custom_id_split[2],
            "language": custom_id_split[3],
            "content": hit.payload["content"], 
            "score": hit.score
        }
        results.append(str(result))
    return results
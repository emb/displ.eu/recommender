import json
import torch
from qdrant_client import QdrantClient, models
from sentence_transformers import SentenceTransformer
from settings import DB_CONNECTION_STRING, DB_NAME, VECTOR_DB_NAME


def import_text_db():
    client = QdrantClient(url=DB_CONNECTION_STRING)
    database = DB_NAME

    # # Prepare your documents, metadata, and IDs
    metadata = [
        {"source": "Langchain-docs"},
        {"source": "Linkedin-docs"},
    ]
    ids = [42, 2]
    docs = ["Qdrant has Langchain integrations", "Qdrant also has Llama Index integrations"]

    client.add(
        collection_name=DB_NAME,
        documents=docs,
    )

    # Use the new add method
    client.add(
        collection_name=DB_NAME,
        documents=docs,
        metadata=metadata,
        ids=ids
    )
    print('Database imported successfully:', DB_NAME)

# # now search the collection    
# results = client.query(
#     collection_name=DB_NAME, 
#     query_text="This is a query document",
#     limit=10
# )

def import_vector_db(data):
    # Define batch size
    batch_size = 4  # Adjust as necessary
    
    print("CUDA Available:", torch.cuda.is_available())
        
    # Initialize encoder (modify to CPU if needed)
    encoder = SentenceTransformer("all-MiniLM-L6-v2", device='cuda' if torch.cuda.is_available() else 'cpu')

    client = QdrantClient(url=DB_CONNECTION_STRING)
    
    # Create collection
    client.create_collection(
        collection_name=VECTOR_DB_NAME,
        vectors_config=models.VectorParams(
            size=encoder.get_sentence_embedding_dimension(),
            distance=models.Distance.COSINE,
        ),
    )

    # Prepare points
    points = [
        models.PointStruct(
            id=idx,
            vector=encoder.encode(doc["content"]).tolist(),
            payload=doc
        )
        for idx, doc in enumerate(data[:100000])
    ]

    # Upload points in batches
    for i in range(0, len(points), batch_size):
        batch = points[i:i + batch_size]
        try:
            client.upload_points(collection_name=VECTOR_DB_NAME, points=batch)
            # print(f"Data uploaded successfully.")
        except RuntimeError as e:
            print(f"RuntimeError encountered while uploading batch {i // batch_size + 1}: {e}")
            # Optionally, handle retries or log errors for further analysis    
    print(f"Data uploaded successfully.")
      
            
# Load data
with open('vsdb_demo/data/sample_content_item_excluded_cba_updated.json', 'r') as file:
    data = json.load(file)
    
import_vector_db(data)
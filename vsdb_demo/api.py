# API specifications for the recommender system based on Qdrant database

from fastapi import FastAPI
# from settings import DB_NAME
from vsdb_demo.search_db import search_vector_db


app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


# @app.get("/recsys/similar_docs")
# async def similar_docs(query: str, k: int = 5) -> list[str]:
#     # search the Qdrant database for similar documents to the query
#     # return the list of uids which are in the metadata of the results
#     search_results = search_db(query, k)
#     return search_results

@app.get("/recsys/similar_docs")
async def similar_docs(query: str, language: str = "en", k: int = 5) -> list[str]:
    # search the Qdrant database for similar documents to the query
    # return the list of uids which are in the metadata of the results
    search_results = search_vector_db(query, language, k)
    return search_results
# Workpackage/Task

## DISPL.EU 2023

* *Name:* **Recommendation & Commenting**
* *Task-ID:* **T2.4**
* *Participants:*
  * **fairkom**
  * **CBA**
* *ERP:* **[TASK00156](https://erp.fairkom.net/app/task/TASK00156)**
